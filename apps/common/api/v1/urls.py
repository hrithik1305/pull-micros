from django.urls import include, path

from .views import (
    BulkTrackerView,
    DemoTaskSchedulerView,
    ReportSyncPullView,
    TrackingSyncView,
)

urlpatterns = [
    path("bulk-tracker/", BulkTrackerView.as_view(), name="bulk_tracker"),
    path("sync/", TrackingSyncView.as_view()),
    path("demo-task-scheduler-api/", DemoTaskSchedulerView.as_view()),
    path("report-sync-pull/", ReportSyncPullView.as_view()),
]
