from djongo import models

COURIER_TYPE_CHOICES = (
    (1, "DTDC"),
    (2, "DTDC"),
    (3, "BLUEDART"),
    (4, "BLUEDART"),
    (5, "BLUEDART"),
    (6, "BLUEDART"),
    (7, "BLUEDART"),
    (8, "BLUEDART"),
    (9, "BLUEDART"),
    (10, "FEDEX"),
    (11, "FEDEX"),
    (12, "FEDEX"),
    (13, "FEDEX"),
    (14, "FEDEX"),
    (15, "FEDEX"),
    (16, "INDIA POST"),
    (17, "Shree Maruti Courier"),
    (18, "First Flight"),
    (19, "Professional"),
    (20, "Delhivery"),
    (21, "Delhivery"),
    (22, "Delhivery"),
    (23, "Overnite"),
    (24, "GoJavas"),
    (25, "Aramex"),
    (26, "NECC"),
    (27, "DOTZOT"),
    (28, "Holisol"),
    (29, "Innovex"),
    (30, "Velex"),
    (31, "BookMyPacket"),
    (32, "WeDIB"),
    (33, "Xpressbees"),
    (34, "Xpressbees"),
    (35, "Delhivery"),
    (36, "Ecom Express"),
    (37, "Ecom Express"),
    (38, "Delhivery"),
    (39, "Delhivery"),
    (40, "Delhivery"),
    (41, "BookMyPacket"),
    (42, "DotZot"),
    (43, "Xpressbees"),
    (44, "Xpressbees"),
    (45, "Delhivery"),
    (46, "Delhivery"),
    (47, "Delhivery"),
    (48, "Bookmypacket"),
    (49, "Xpressbees"),
    (50, "Delhivery"),
    (51, "Delhivery"),
    (52, "Delhivery"),
    (53, "Delhivery"),
    (54, "Vulcan Courier"),
    (55, "Ekart"),
    (56, "Ekart"),
    (57, "Ekart"),
    (58, "Ekart"),
    (59, "Ecom Express"),
    (60, "ShadowFax"),
    (61, "ShadowFax"),
    (62, "KerryIndev"),
    (63, "Delhivery"),
    (64, "Delhivery"),
    (65, "ParcelDo"),
    (66, "ParcelDo"),
    (67, "MilesAway"),
    (68, "DvcExpress"),
    (69, "Amaze"),
    (70, "Amaze Docs"),
    (71, "Udaan Express"),
    (72, "Udaan Express"),
)


class FailedTracking(models.Model):
    _id = models.ObjectIdField()
    awb = models.CharField(max_length=128, unique=True)
    courier_type = models.IntegerField(choices=COURIER_TYPE_CHOICES)
    attempts = models.PositiveIntegerField(default=0)
    order_created_at = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = models.DjongoManager()

    def __str__(self):
        return self.awb


class Status(models.Model):
    scan_status = models.CharField()
    scan_type = models.CharField()
    scan_datetime = models.DateTimeField()
    scan_location = models.CharField()

    class Meta:
        abstract = True

    def __str__(self):
        return self.scan_status


class TrackingInfo(models.Model):

    default_empty = {"null": True, "blank": True}

    ZONE_CHOICES = (
        ("A", "Zone A"),
        ("B", "Zone B"),
        ("C", "Zone C"),
        ("D", "Zone D"),
        ("E", "Zone E"),
    )

    _id = models.ObjectIdField()
    auth_token = models.CharField(max_length=128, **default_empty)
    tracking_id = models.CharField(max_length=128, unique=True, db_index=True)
    courier_tracking_id = models.CharField(
        max_length=128, unique=True, db_index=True
    )
    return_waybil = models.CharField(max_length=128, **default_empty)
    pickrr_order_id = models.CharField(max_length=128, **default_empty)
    client_order_id = models.CharField(max_length=128, **default_empty)
    courier_parent_name = models.CharField(max_length=128, **default_empty)
    courier_used = models.CharField(max_length=128, **default_empty)
    length = models.IntegerField(default=0, **default_empty)
    breadth = models.IntegerField(default=0, **default_empty)
    height = models.IntegerField(default=0, **default_empty)
    weight = models.FloatField(default=0.0, **default_empty)
    is_cod = models.BooleanField(**default_empty)
    item_tax_percentage = models.TextField(**default_empty)
    web_address = models.CharField(max_length=256, **default_empty)
    billing_zone = models.CharField(
        max_length=64, choices=ZONE_CHOICES, **default_empty
    )
    dispatch_mode = models.CharField(max_length=128, **default_empty)
    logo = models.TextField(**default_empty)
    sku = models.TextField(**default_empty)
    item_list = models.JSONField(default=list, **default_empty)
    user_id = models.IntegerField(**default_empty)
    order_type = models.CharField(max_length=128, **default_empty)
    hsn_code = models.CharField(max_length=128, **default_empty)
    company_name = models.CharField(max_length=256, **default_empty)
    product_name = models.CharField(max_length=256, **default_empty)
    status = models.JSONField(default=dict, **default_empty)
    client_extra_var = models.CharField(max_length=256, **default_empty)
    is_reverse = models.BooleanField(**default_empty)
    info = models.JSONField(default=dict, **default_empty)
    err = models.CharField(max_length=128, **default_empty)
    track_arr = models.JSONField(default=list, **default_empty)
    edd_stamp = models.CharField(max_length=128, **default_empty)
    quantity = models.IntegerField(default=0, **default_empty)
    show_details = models.BooleanField(default=False, **default_empty)
    order_created_at = models.DateTimeField(**default_empty)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = models.DjongoManager()

    def __str__(self):
        return self.tracking_id


class CourierResponse(models.Model):
    default_empty = {"null": True, "blank": True}

    _id = models.ObjectIdField()
    tracking_id = models.CharField(max_length=128, unique=True, db_index=True)
    courier_tracking_id = models.CharField(
        max_length=128, unique=True, db_index=True
    )
    courier_response = models.JSONField(default=dict, **default_empty)
    pickrr_response = models.JSONField(default=dict, **default_empty)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = models.DjongoManager()

    def __str__(self):
        return self.tracking_id
