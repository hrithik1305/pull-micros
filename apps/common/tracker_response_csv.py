import csv

from apps.udaan.services import tracker

couriers = [
    "dtdc",
    "dtdc_air",
    "bluedart",
    "bluedart_dart_plus",
    "bluedart_air_zookr",
    "bluedart_dg_zookr",
    "bluedart_ras_ncr",
    "bluedart_ras_external",
    "bluedart_air_yn_mumbai_chen_hyd_bang",
    "fedex",
    "fedex_po",
    "fedex_economy",
    "fedex_so",
    "fedex_3kg",
    "fedex_3kg_surface",
    "delhivery",
    "delhivery_express",
    "delhivery_non_est",
    "xpressbees",
    "xpressbees_non_est",
    "delhivery_air",
    "ecommexpress",
    "ecommexpress_ras",
    "delhivery_bulk",
    "delhivery_air_zookr",
    "delhivery_dg_zookr",
    "xpressbees_zookr",
    "xpressbees_weshyp",
    "weshyp_delhivery_air_bulk",
    "weshyp_delhivery_air_express",
    "weshyp_delhivery_express",
    "weshyp_xpressbees",
    "delhivery_heavy",
    "delhivery_heavy_mps",
    "delhivery_bulk_mps",
    "delhivery_ncr_mps",
    "ekart",
    "ekart_new",
    "ekart_new_non_est",
    "ekart_new_surface",
    "ecomm_new",
    "shadowfax",
    "shadowfax_bulk",
    "kerry_indev",
    "delhivery_5kg",
    "delhivery_5kg_mps",
    "parceldo",
    "parceldo_sdd",
    "amaze",
    "amaze_docs",
    "udaan",
    "udaan_b2b",
]

courier_data = {}
file = "tracking_ids.csv"
with open(file) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=",")
    for row in csv_reader:
        if row[1] in couriers:
            if courier_data.get(row[1]):
                courier_data[row[1]].append(row[2])
            else:
                courier_data[row[1]] = [row[2]]

courier_key = "udaan"
file_name = "{}.csv".format(courier_key)
with open(file_name, mode="w") as courier_data_file:
    courier_writer = csv.writer(
        courier_data_file,
        delimiter=",",
        quotechar='"',
        quoting=csv.QUOTE_MINIMAL,
    )
    courier_writer.writerow(
        ["tracking_ids", "courier_response", "pickrr_response", "courier"]
    )

    chunk_size = 10
    for i in range(0, len(courier_data[courier_key]), chunk_size):
        chunked_list = courier_data[courier_key][i : i + chunk_size]
        response = tracker(courier_key, chunked_list)
        try:
            courier_writer.writerow(
                [
                    chunked_list,
                    response["data"]["courier_response"],
                    response["data"]["pickrr_response"],
                    courier_key,
                ]
            )
        except Exception as e:
            print(e)

    # with open('courier_data.csv', mode='w') as courier_data_file:
    #     courier_writer = csv.writer(courier_data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    #     courier_writer.writerows([[i,j] for i,j in courier_data.items()])
