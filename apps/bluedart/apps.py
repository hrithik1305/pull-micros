from django.apps import AppConfig


class BluedartConfig(AppConfig):
    name = "apps.bluedart"
