from datetime import datetime

from pickrr_tracker.loggers import amaze_logger

amaze_code_mapper = {
    "Soft Data Upload": {"scan_type": "OP", "pickrr_sub_status_code": ""},
    "In Scan – HUB": {"scan_type": "PP", "pickrr_sub_status_code": ""},
    "Bag Created For HUB": {"scan_type": "SHP", "pickrr_sub_status_code": ""},
    "Bag Created For BRANCH": {
        "scan_type": "SHP",
        "pickrr_sub_status_code": "",
    },
    "In Transit To HUB": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "In Transit To BRANCH": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "Bag Verified At HUB": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "Bag Verified At BRANCH": {
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "Shipment Received At HUB": {
        "scan_type": "RAD",
        "pickrr_sub_status_code": "",
    },
    "Shipment Received At BRANCH": {
        "scan_type": "RAD",
        "pickrr_sub_status_code": "",
    },
    "Out for Delivery": {"scan_type": "OO", "pickrr_sub_status_code": ""},
    "Undelivered-COD Not Ready": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNR",
    },
    "Undelivered-House Locked": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNA",
    },
    "Undelivered-Customer Asking For Future Delivery On YYYY-MM-DD": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CD",
    },
    "Undelivered-Out Of Delivery Area": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "ODA",
    },
    "Undelivered-Misroute": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "SD",
    },
    "Undelivered-Address Not Located And Customer Not Contactable": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNA",
    },
    "Undelivered-Customer Forcibly Open The Shipment And Refused To Accept The Delivery": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CR",
    },
    "Undelivered-Order Cancelled By Customer": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CR",
    },
    "Undelivered-Vehicle Breakdown Could Not Attempt": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "SD",
    },
    "Undelivered-COD Amount Mismatch": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNR",
    },
    "Undelivered-Address Pincode Mismatch": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "AI",
    },
    "Undelivered-Shipment Lost": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "OTH",
    },
    "Undelivered-Incomplete Address": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "AI",
    },
    "Undelivered-Customer Not Contactable": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNA",
    },
    "Undelivered-Order Cancelled By Client": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CR",
    },
    "Delivered": {"scan_type": "DL", "pickrr_sub_status_code": ""},
    "Process to be RTO": {"scan_type": "RTO", "pickrr_sub_status_code": ""},
    "RTO Initiated": {"scan_type": "RTO", "pickrr_sub_status_code": ""},
    "RTO - Pending To Transit": {
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "RTO - Transit to HUB": {
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "RTO - Received By HUB": {
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "RTO OFD": {"scan_type": "RTO-OO", "pickrr_sub_status_code": ""},
    "RTO Undelivered": {"scan_type": "RTO UD", "pickrr_sub_status_code": ""},
    "RTO Delivered": {"scan_type": "RTD", "pickrr_sub_status_code": ""},
}


def get_amaze_tracking_details(response, waybill):
    tracking_dict = {}
    try:
        pickup_datetime = None
        edd_stamp = None
        if "data" in response and response["data"]:
            tracking_array = []
            for detail in response["data"]["history"]:
                track_details = {}
                if detail["remarks"]:
                    status_string = detail["status"] + "-" + detail["remarks"]
                else:
                    status_string = detail["status"]
                if str(status_string) in amaze_code_mapper:
                    track_details["scan_type"] = amaze_code_mapper[
                        status_string
                    ]["scan_type"]
                else:
                    continue
                amaze_datetime = detail["date"]
                status_time = datetime.strptime(
                    amaze_datetime, "%Y-%m-%d %I:%M %p"
                )
                from apps.common.services import ist_to_utc_converter

                status_time = ist_to_utc_converter(status_time)
                track_details["scan_datetime"] = status_time
                track_details["scan_status"] = str(detail["remarks"])
                track_details["scan_location"] = detail["location"]
                track_details["pickrr_sub_status_code"] = amaze_code_mapper[
                    status_string
                ]["pickrr_sub_status_code"]
                track_details["courier_status_code"] = status_string
                tracking_array.append(track_details.copy())
                if track_details["scan_type"] == "PP":
                    pickup_datetime = status_time
            if response.get("eta", None) is not None:
                edd_stamp = response["eta"]
                edd_stamp = datetime.strptime(edd_stamp, "%Y-%m-%d %I:%M %p")
                edd_stamp = ist_to_utc_converter(edd_stamp)
            tracking_dict["edd_stamp"] = edd_stamp
            if len(tracking_array) > 0:
                tracking_dict["received_by"] = ""
                tracking_dict["awb"] = str(waybill)
                if tracking_array[0]["scan_type"] == "DL":
                    tracking_dict["received_by"] = (
                        str(response["received_by"])
                        if response.get("received_by", None) is not None
                        else ""
                    )
                tracking_dict["status_date"] = tracking_array[0][
                    "scan_datetime"
                ]
                tracking_dict["status_info"] = tracking_array[0]["scan_status"]
                tracking_dict["status"] = tracking_array[0]["scan_type"]
                if tracking_array[0]["scan_type"] == "NDR":
                    tracking_dict["status_type"] = "OT"
                else:
                    tracking_dict["status_type"] = tracking_array[0][
                        "scan_type"
                    ]
                tracking_dict["pickup_datetime"] = pickup_datetime
                tracking_dict["tracking_array"] = tracking_array
        else:
            tracking_dict["awb"] = str(waybill)
            tracking_dict["err"] = str(response.get("error", "unknown"))
            amaze_logger.error(
                {"err": tracking_dict["err"], "waybill": waybill}
            )
        return tracking_dict
    except Exception as e:
        amaze_logger.error(
            {
                "err": str(e),
                "waybill": waybill,
                "courier_response": str(response),
            }
        )
        return {"tracking_id": str(waybill), "err": str(e)}
