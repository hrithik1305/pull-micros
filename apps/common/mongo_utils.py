import pymongo
from decouple import config

from pickrr_tracker.loggers import responseupdate_logger


def get_pymongo_client():
    mongo_url = config("DB_HOST")
    mongo_client = pymongo.MongoClient(mongo_url)
    return mongo_client


def get_pymongo_db_using_client(mongo_client):
    mongo_db_name = config("DB_NAME")
    client_db = mongo_client[mongo_db_name]
    return client_db


def get_pymongo_col_using_db(db, collection_name):
    db_collection_name = collection_name
    db_collection = db[db_collection_name]
    return db_collection


def create_document_to_mongo_db_using_pymongo(obj_dict, collection_name):
    try:
        mongo_client = get_pymongo_client()
        client_db = get_pymongo_db_using_client(mongo_client)
        db_colllection = get_pymongo_col_using_db(client_db, collection_name)

        try:
            doc_id = db_colllection.insert_one(obj_dict)
        except Exception as e:
            raise e

        return doc_id.inserted_id

    except Exception as e:
        # TODO: log this error
        raise e


def fetch_documents_from_mongo_using_filters_and_values(
    filters, values, collection_name
):
    try:
        return batch_document_iterator(filters, values, collection_name)
    except Exception as e:
        raise e


def update_one_document(filters, update_dict, col_name):
    try:
        mongo_client = get_pymongo_client()
        client_db = get_pymongo_db_using_client(mongo_client)
        db_colllection = get_pymongo_col_using_db(client_db, col_name)

        update_query_return_val = db_colllection.update_one(
            filter=filters, update=update_dict
        )
        return update_query_return_val
    except Exception as e:
        raise e


def update_multiple_document(query_list, tracking_col_name):
    from pymongo import UpdateOne

    try:
        mongo_client = get_pymongo_client()
        client_db = get_pymongo_db_using_client(mongo_client)
        db_colllection = get_pymongo_col_using_db(client_db, tracking_col_name)
        request = []
        for query in query_list:
            request.append(
                UpdateOne(
                    {"courier_tracking_id": query["courier_tracking_id"]},
                    {"$set": query["$set"]},
                    upsert=True,
                )
            )
        from django.template.defaultfilters import pprint
        from pymongo.errors import BulkWriteError

        try:
            if len(request) != 0:
                db_colllection.bulk_write(request, ordered=False)
        except BulkWriteError as bwe:
            responseupdate_logger.error(
                {"err": str(bwe.details), "request": str(request)}
            )
            pprint(bwe.details)
    except Exception as e:
        responseupdate_logger.error({"err": str(e), "query_list": query_list})
        raise e


def replace_one_document(filters, updated_document, col_name):
    try:
        mongo_client = get_pymongo_client()
        client_db = get_pymongo_db_using_client(mongo_client)
        db_colllection = get_pymongo_col_using_db(client_db, col_name)
        update_query_return_val = db_colllection.replace_one(
            filter=filters, replacement=updated_document
        )

        return update_query_return_val
    except Exception as e:
        raise e


def update_or_create_document_in_mongo(obj_dict, col_name, filters):
    try:
        mongo_client = get_pymongo_client()
        client_db = get_pymongo_db_using_client(mongo_client)
        db_collection = get_pymongo_col_using_db(client_db, col_name)
        doc_id = db_collection.find_one_and_update(
            filter=filters, update=obj_dict, upsert=True
        )
        return doc_id
    except Exception as e:
        raise e


def batch_document_iterator(filters, values, col_name, batch=5000):
    mongo_client = get_pymongo_client()
    client_db = get_pymongo_db_using_client(mongo_client)
    db_collection = get_pymongo_col_using_db(client_db, col_name)
    count_objs = db_collection.find(filters, values).count()
    for i in range(0, count_objs, batch):
        doc_list = list(
            db_collection.find(filters, values).sort(
                [("updated_at", pymongo.ASCENDING)]
            )[i : i + batch]
        )
        yield doc_list


def batch_documents_with_agg(filters, col_name, batch=5000):
    mongo_client = get_pymongo_client()
    client_db = get_pymongo_db_using_client(mongo_client)
    db_collection = get_pymongo_col_using_db(client_db, col_name)

    return db_collection.aggregate(filters)


def get_db_collection(col_name):
    mongo_client = get_pymongo_client()
    client_db = get_pymongo_db_using_client(mongo_client)
    db_collection = get_pymongo_col_using_db(client_db, col_name)

    return db_collection


def get_pymongo_source():
    mongo_url = config("DB_SOURCE_HOST")
    mongo_client = pymongo.MongoClient(mongo_url)
    return mongo_client


def get_pymongo_source_db_using_client(mongo_client):
    mongo_db_name = config("DB_SOURCE_DB_NAME")
    client_db = mongo_client[mongo_db_name]
    return client_db


def get_pymongo_source_col_using_db(db, collection_name):
    db_collection_name = collection_name
    db_collection = db[db_collection_name]
    return db_collection


def batch_source_mongo_document_iterator(
    filters, collection_name, values=None, batch=90000
):
    mongo_client = get_pymongo_source()
    client_db = get_pymongo_source_db_using_client(mongo_client)
    db_collection = get_pymongo_source_col_using_db(client_db, collection_name)

    count_objs = db_collection.find(filters).count()
    for i in range(0, count_objs, batch):
        if values is None:
            doc_list = list(db_collection.find(filters)[i : i + batch])
        else:
            doc_list = list(db_collection.find(filters, values)[i : i + batch])
        yield doc_list


def fetch_single_document_from_mongo_using_filters_and_values(
    filters, values, collection_name
):
    mongo_client = get_pymongo_client()
    client_db = get_pymongo_db_using_client(mongo_client)
    db_collection = get_pymongo_col_using_db(client_db, collection_name)

    try:
        if values:
            pymongo_cursor_obj = db_collection.find(filters, values)
        else:
            pymongo_cursor_obj = db_collection.find(filters)
        doc = pymongo_cursor_obj[0]
        return doc
    except Exception as e:
        return None


def get_single_document(filters={}, values=None, collection_name=""):
    mongo_client = get_pymongo_client()
    client_db = get_pymongo_db_using_client(mongo_client)
    db_collection = get_pymongo_col_using_db(client_db, collection_name)

    return db_collection.find_one(filters, values)
