BASE_TRACKING_URL = "https://www.pickrr.com/tracking"

CHANNELS = (
    ('sms', 'SMS'),
    ('wp', 'Whatsapp'),
    ('email', 'Email'),
)

EVENTS_STATUS_MAP = {
    "OP" : "order_placed",
    "PP" : "order_picked_up",
    "OT" : "order_in_transit",
    "DD" : "delivery_delay",
    "OO" : "out_for_delivery_sd",
    "OOE" : "out_for_delivery_er",
    "DL" : "order_delivered",
    "NDR": "order_not_delivered",
    "OC" : "order_cancelled",
    "PUD" : "picked_up_delayed",
}

EMAIL_TEMPLATES = {
    "OP": "notification/order_placed.html",
    "PP": "notification/order_dispathced.html",
    "OO": "notification/out_for_delivery.html",
    "DL": "notification/delivered.html",
    "NDR": "notification/ndr.html",
    "OT": "notification/in_transit.html",
    "DD": "notification/delivery_delayed.html",
    "OOE": "notification/order_arriving_early.html",
    "OC": "notification/order_canceled.html",
    "PUD": "notification/order_delayed.html",
}

STATUS_SUBJECT_MAPS = {
    "OP": "Order Placed",
    "PP": "Order Picked Up",
    "OT": "Order In Transit",
    "DD": "Delay Delivery",
    "OO": "Out For Delivery",
    "OOE": "Early Out For Delivery",
    "DL": "Order Delivered",
    "NDR": "Order Not Delivered",
    "OC": "Order Cancelled",
    "PUD": "Pickup Delayed",
}

WP_TEMPLATE_NAME_MAPS = {
    "OC": "r_order_cancelled",
    "DL": "r_delivered",
    "OOE": "r_ofd_early",
    "OO": "r_ofd_standard",
    "DD": "r_delayed_delivery",
    "OT": "s_order_in_transit",
    "PUD": "r_pickup_delayed",
    "PP": "r_order_pickedup",
    "OP": "r_order_placed",
    "NDR": "r_non_delivered",
}


NOTIFICATION_USER_DEFAULT_DICT = {
    "email": "",
    "order_placed": [],
    "order_picked_up": [],
    "picked_up_delayed": [],
    "order_in_transit": [],
    "delivery_delay": [],
    "out_for_delivery_sd": [],
    "out_for_delivery_er": [],
    "order_delivered": [],
    "order_not_delivered": [],
    "order_cancelled": [],
    "free_wp_events": [],
    "free_sms_events": [],
    "coins_to_rupee": 0.15,
    "delay_delivery_in_days": 1,
    "total_coins": 10000, # after Trail period, set to 0
    "sms_coins": 0.15,
    "wp_coins": 0.45,
    "email_coins": 0.0,
    "free_sms": 2,
    "free_wp": 1,
    "free_email": -1,
    "is_active": True,
    "kwargs": {},
    "logs": [],
}