from django.conf import settings

from apps.common.mongo_utils import (
    create_document_to_mongo_db_using_pymongo,
    get_single_document, create_document_to_mongo_db_using_pymongo,
    update_one_document, batch_document_iterator
)

class NotificationDao:

    COLLECTION_NAME = settings.USER_NOTIFICATION_COLLECTION_NAME
    LOGS_COLLECTION_NAME = settings.NOTIFICATION_LOGS_COLLECTION_NAME
    
    @classmethod
    def get_notification_user(cls, filter):
        document = get_single_document(filter, values={'logs': 0}, collection_name=cls.COLLECTION_NAME)   
        return document  
    

    @classmethod
    def get_notification_logs(cls, filter, type='single'):
        if type == 'single':
            document = get_single_document(filter, values=None, collection_name=cls.LOGS_COLLECTION_NAME)   
            return document  
        return batch_document_iterator(filter, None, cls.LOGS_COLLECTION_NAME)
    

    @classmethod
    def create_logs(cls, obj):
        document = create_document_to_mongo_db_using_pymongo(obj, cls.LOGS_COLLECTION_NAME) 
        return document
    
    @classmethod 
    def update_logs(cls, filter, obj):
        updated_obj ={
            "$set": obj
        }
        document = update_one_document(filter, updated_obj, cls.LOGS_COLLECTION_NAME)
        return document
    
    @classmethod 
    def update_user_data(cls, filter, obj):
        updated_obj ={
            "$set": obj
        }
        document = update_one_document(filter, updated_obj, cls.COLLECTION_NAME)
        return document
    
    @staticmethod
    def tracking_events(**kwargs):
        pass
