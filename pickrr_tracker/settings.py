"""
Django settings for pickrr_tracker project.

Generated by 'django-admin startproject' using Django 3.2.5.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.2/ref/settings/
"""
import os
from pathlib import Path

import sentry_sdk
from decouple import Csv, config
from sentry_sdk.integrations.django import DjangoIntegration

from .logging_settings import logging_config

# Build paths inside the project like this: BASE_DIR / 'subdir'.

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = config("SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = config("DEBUG", cast=bool)

ALLOWED_HOSTS = config("ALLOWED_HOSTS", cast=Csv())
LOGGING = logging_config

if not DEBUG:
    # Sentry
    sentry_sdk.init(
        dsn="https://c1d16cbaf5f94828a62b29eaff44ef18@o792256.ingest.sentry.io/5939180",
        integrations=[DjangoIntegration()],
        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        # We recommend adjusting this value in production.
        traces_sample_rate=1.0,
        # If you wish to associate users to errors (assuming you are using
        # django.contrib.auth) you may enable sending PII data.
        send_default_pii=True,
    )

# Application definition

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "corsheaders",
    "graphene_django",
    "apps.delhivery",
    "apps.ekart",
    "apps.shadowfax",
    "apps.bluedart",
    "apps.ecomm",
    "apps.xpressbees",
    "apps.kerryindev",
    "apps.common",
    "apps.fedex",
    "apps.parceldo",
    "apps.dtdc",
    "apps.amaze",
    "rest_framework",
    "apps.udaan",
    "pickrr_tracker",
    "django_celery_beat",
    "apps.notification"
]

MIDDLEWARE = [
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "pickrr_tracker.urls"
CORS_ALLOW_HEADERS = [
    'accept',
    'accept-encoding',
    'authorization',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
    'x-email'
]

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "pickrr_tracker.wsgi.application"

# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "djongo",
        "NAME": config("DB_NAME"),
        "CLIENT": {
            "username": config("DB_USER"),
            "password": config("DB_PASSWORD"),
            "host": config("DB_HOST"),
            "authMechanism": "SCRAM-SHA-1",
        },
    }
}

# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]


REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    "DEFAULT_PERMISSION_CLASSES": []
}

# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/

STATIC_URL = "/static/"

STATIC_ROOT = os.path.join(BASE_DIR, "static/")

# CELERY STUFF
CELERY_BROKER_URL = "redis://localhost:6379"
CELERY_RESULT_BACKEND = "redis://localhost:6379"
CELERY_ACCEPT_CONTENT = ["application/json"]
CELERY_TASK_SERIALIZER = "json"
CELERY_RESULT_SERIALIZER = "json"
CELERY_TIMEZONE = "Asia/Kolkata"

# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

DEFAULT_REQUESTS_TIMEOUT = 10  # 5 seconds

DELHIVERY_TOKEN = config("DELHIVERY_TOKEN")
DELHIVERY_NCR_MPS_TOKEN = config("DELHIVERY_NCR_MPS_TOKEN")
DELHIVERY_NON_EST_TOKEN = config("DELHIVERY_NON_EST_TOKEN")
DELHIVERY_EXPRESS_TOKEN = config("DELHIVERY_EXPRESS_TOKEN")
DELHIVERY_REVERSE_TOKEN = config("DELHIVERY_REVERSE_TOKEN")
DELHIVERY_AIR_TOKEN = config("DELHIVERY_AIR_TOKEN")
DELHIVERY_BULK_TOKEN = config("DELHIVERY_BULK_TOKEN")
DELHIVERY_BULK_MPS_TOKEN = config("DELHIVERY_BULK_MPS_TOKEN")
DELHIVERY_5KG_TOKEN = config("DELHIVERY_5KG_TOKEN")
DELHIVERY_5KG_MPS_TOKEN = config("DELHIVERY_5KG_MPS_TOKEN")
DELHIVERY_AIR_ZOOKR_TOKEN = config("DELHIVERY_AIR_ZOOKR_TOKEN")
DELHIVERY_DG_ZOOKR_TOKEN = config("DELHIVERY_DG_ZOOKR_TOKEN")
WESHYP_DELHIVERY_AIR_BULK_TOKEN = config("WESHYP_DELHIVERY_AIR_BULK_TOKEN")
WESHYP_DELHIVERY_AIR_EXPRESS_TOKEN = config(
    "WESHYP_DELHIVERY_AIR_EXPRESS_TOKEN"
)
WESHYP_DELHIVERY_EXPRESS_TOKEN = config("WESHYP_DELHIVERY_EXPRESS_TOKEN")
DELHIVERY_HEAVY_TOKEN = config("DELHIVERY_HEAVY_TOKEN")
DELHIVERY_HEAVY_MPS_TOKEN = config("DELHIVERY_HEAVY_MPS_TOKEN")
DELHIVERY_API_WAYBIILS_LIMIT = 75

BLUEDART_LOGIN_ID = config("BLUEDART_LOGIN_ID")
BLUEDART_LIC_KEY = config("BLUEDART_LIC_KEY")

ECOMM_USERNAME = config("ECOMM_USERNAME")
ECOMM_PASSWORD = config("ECOMM_PASSWORD")

XPRESSBEES_TOKEN = config("XPRESSBEES_TOKEN")
XPRESSBEES_NON_EST_TOKEN = config("XPRESSBEES_NON_EST_TOKEN")
XPRESSBEES_ZOOKR_TOKEN = config("XPRESSBEES_ZOOKR_TOKEN")
XPRESSBEES_WESHYP_TOKEN = config("XPRESSBEES_WESHYP_TOKEN")
XPRESSBEES_REVERSE_TOKEN = config("XPRESSBEES_REVERSE_TOKEN")
XPRESSBEES_2KG_TOKEN = config("XPRESSBEES_2KG_TOKEN")
XPRESSBEES_5KG_TOKEN = config("XPRESSBEES_5KG_TOKEN")
XPRESSBEES_10KG_TOKEN = config("XPRESSBEES_10KG_TOKEN")
XPRESSBEES_20KG_TOKEN = config("XPRESSBEES_20KG_TOKEN")

SHADOWFAX_TOKEN = config("SHADOWFAX_TOKEN")

EKART_TOKEN = config("EKART_TOKEN")
EKART_NEW_TOKEN = config("EKART_NEW_TOKEN")
EKART_NEW_NON_EST_TOKEN = config("EKART_NEW_NON_EST_TOKEN")
EKART_NEW_SURFACE_TOKEN = config("EKART_NEW_SURFACE_TOKEN")

FEDEX_KEY = config("FEDEX_KEY")
FEDEX_PASSWORD = config("FEDEX_PASSWORD")
FEDEX_ACCOUNT_NUMBER = config("FEDEX_ACCOUNT_NUMBER")
FEDEX_METER_NUMBER = config("FEDEX_METER_NUMBER")

FEDEX_SURFACE_KEY = config("FEDEX_SURFACE_KEY")
FEDEX_SURFACE_PASSWORD = config("FEDEX_SURFACE_PASSWORD")
FEDEX_SURFACE_ACCOUNT_NUMBER = config("FEDEX_SURFACE_ACCOUNT_NUMBER")
FEDEX_SURFACE_METER_NUMBER = config("FEDEX_SURFACE_METER_NUMBER")

SHADOWFAX_TOKEN = config("SHADOWFAX_TOKEN")
SHADOWFAX_BULK_TOKEN = config("SHADOWFAX_BULK_TOKEN")
SHADOWFAX_BULK_REVERSE_TOKEN = config("SHADOWFAX_BULK_REVERSE_TOKEN")
SHADOWFAX_REVERSE_TOKEN = config("SHADOWFAX_REVERSE_TOKEN")

UDAAN_TOKEN = config("UDAAN_TOKEN")
UDAAN_B2B_TOKEN = config("UDAAN_B2B_TOKEN")

DTDC_ACCESS_TOKEN = config("DTDC_ACCESS_TOKEN")
DTDC_DOCS_250_ACCESS_TOKEN = config("DTDC_DOCS_250_ACCESS_TOKEN")
DTDC_DOCS_100_ACCESS_TOKEN = config("DTDC_DOCS_100_ACCESS_TOKEN")

PARCELDO_TOKEN = config("PARCELDO_TOKEN")

AUTH_KEY = config("AUTH_KEY")

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://localhost:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        },
    }
}

# Notification settings
from .notification_settings import *