from rest_framework import status
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.common.courier_mapping import courier_tracker_map
from apps.common.services import tracker
from apps.common.tasks import *
from apps.common.utils import read_post_json

from .serializers import BulkTrackerSerializer


class BulkTrackerView(APIView):
    def post(self, request, format=None):
        serializer = BulkTrackerSerializer(data=request.data)
        if serializer.is_valid():
            courier_type = serializer.validated_data.get("courier_type")
            tracking_ids = serializer.validated_data.get("tracking_ids")
            # orders_created_at = serializer.validated_data.get(
            #     "orders_created_at"
            # )
            res = tracker(courier_type, tracking_ids)
            return Response(res)

        return Response(
            {"is_success": False, "msg": None, "data": serializer.errors}
        )


class TrackingSyncView(APIView):
    """
    API to receive data from v1 on order placing,
    """

    def post(self, request: Request):
        params = read_post_json(request.body)
        from apps.common.tasks import tracking_sync_task

        tracking_sync_task.delay(params)
        # res = service.tracking_sync(params)
        return Response({"success": True}, status=status.HTTP_200_OK)


class ReportSyncPullView(APIView):
    """
    API to sync data from report mongo to pull mongo
    """

    def post(self, request: Request):
        params = read_post_json(request.body)
        from apps.common.tasks import report_mongo_sync_pull_mongo_task

        report_mongo_sync_pull_mongo_task.delay(params)
        # res = service.tracking_sync(params)
        return Response({"success": True}, status=status.HTTP_200_OK)


class DemoTaskSchedulerView(APIView):
    """
    Demo API to start pulling data on server
    """

    def get(self, request: Request):
        from apps.common.services import fetch_updates_from_courier

        fetch_updates_from_courier()
        return Response({"success": True}, status=status.HTTP_200_OK)
