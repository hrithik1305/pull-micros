import json

from django.conf import settings

from apps.common.utils import POSTRequest, xml_to_dict
from pickrr_tracker.loggers import tci_logger

from .utils import prepare_track_data_tci


def tci_tracker(courier_type, waybills: list) -> dict:
    final_tracking_array = []
    final_courier_response = []
    try:
        url = "https://customerapi.tciexpress.in/ServiceEnquire.asmx"
        headers = {"Content-Type": "text/xml"}
        for waybill in waybills:
            data = {"trkType": "cnno", "strcnno": waybill, "addtnlDtl": "Y"}
            payload = """<?xml version="1.0" encoding="utf-8"?>
                        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                            <soap:Body>
                            <getConsignmentResponseMessage xmlns="http://www.tciexpress.in/">
                                <pConsignmentNumber>%s</pConsignmentNumber>
                                <pUserProfile>
                                <UserID>%s</UserID>
                                <Password>%s</Password>
                                </pUserProfile>
                            </getConsignmentResponseMessage>
                            </soap:Body>
                        </soap:Envelope>""" % (
                waybill,
                settings.TCI_USER,
                settings.TCI_PASSWORD,
            )

            postObj = POSTRequest(
                url=url,
                data=payload,
                xml=True,
                headers=headers,
                json_dumps=False,
            )
            response = postObj.send()
            response["data"]["courier_response"] = json.loads(
                response["data"]["courier_response"]
            )
            tci_logger.info(
                {
                    "url": url,
                    "data": data,
                    "courier_response": str(response),
                }
            )
            print(response)
            final_courier_response.append(response["data"]["courier_response"])
            if response["is_success"]:
                try:
                    tracking_dict = prepare_track_data_tci(waybill, response)
                except Exception as e:
                    tci_logger.error({"err": str(e), "waybill": waybill})
                    tracking_dict = {"err": e}
                final_tracking_array.append(tracking_dict)

        response["data"]["courier_response"] = final_courier_response
        response["data"]["pickrr_response"] = final_tracking_array

        return response
    except Exception as e:
        tci_logger.error({"err": str(e), "awbs": waybills})
        return {"err": str(e), "awbs": waybills}
