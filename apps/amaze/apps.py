from django.apps import AppConfig


class AmazeConfig(AppConfig):
    name = "apps.amaze"
