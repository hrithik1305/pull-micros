from django.conf import settings

from apps.common.utils import POSTRequest
from pickrr_tracker.loggers import shadowfax_logger


def tracker(courier_type, waybills: list) -> dict:
    data = {
        "awb_numbers": ",".join(waybills),
    }
    url = "http://dale.shadowfax.in/api/v2/clients/bulk_track/?format=json"
    headers = {
        "content-type": "application/json",
        "Authorization": "Token {}".format(settings.SHADOWFAX_TOKEN),
    }
    request = POSTRequest(url=url, data=data, headers=headers)
    response = request.send()
    shadowfax_logger.info(
        {"url": url, "data": data, "courier_response": str(response)}
    )
    return response
