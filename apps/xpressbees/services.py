from apps.common.utils import POSTRequest
from apps.xpressbees.utils import xpressbees_reverse_tracker
from pickrr_tracker.loggers import xpressbees_logger

from .utils import get_pkr_response_xbs, get_token


def tracker(courier_type: str, waybills: list) -> dict:
    params = {"AWBNo": ",".join(waybills), "XBkey": get_token(courier_type)}
    headers = {"Content-Type": "application/json;charset=UTF-8"}
    url = "http://xbclientapi.xpressbees.com/PickrrService.svc/GetShipmentSummaryDetails"
    request = POSTRequest(url, data=params, headers=headers)
    response = request.send()
    xpressbees_logger.info(
        {"url": url, "params": params, "courier_response": str(response)}
    )
    if response["is_success"]:
        try:
            response["data"]["pickrr_response"] = get_pkr_response_xbs(
                response["data"]["courier_response"]
            )
        except Exception as e:
            xpressbees_logger.error({"err": str(e)})
            response["data"]["pickrr_response"] = {"err": e}
    return response


def reverse_tracker(courier_type: str, waybills: list) -> dict:
    final_tracking_array = []
    url = "http://xbclientapi.xpressbees.com/PickrrService.svc/GetReverseShipmentSummaryDetails"
    try:
        for waybill in waybills:
            jsondata = {"AWBNo": waybill}
            headers = {
                "Content-Type": "application/json;charset=UTF-8",
                "VersionNumber": "V1",
                "XBkey": get_token(courier_type),
            }
            request = POSTRequest(url=url, data=jsondata, headers=headers)
            res = request.send()
            if res["is_success"]:
                tracking_dict = xpressbees_reverse_tracker(
                    res["data"], waybill
                )
                final_tracking_array.append(tracking_dict)
        return final_tracking_array
    except Exception as e:
        return {"err": str(e)}
