from django.conf import settings
from rest_framework import serializers

from apps.common.courier_mapping import courier_tracker_map_dict


class BulkTrackerSerializer(serializers.Serializer):
    courier_type = serializers.CharField()
    tracking_ids = serializers.ListField(child=serializers.CharField())
    # orders_created_at = serializers.ListField(
    #     child=serializers.DateTimeField(
    #         format="%Y-%m-%d %H:%M:%S",
    #         input_formats=["%Y-%m-%d %H:%M:%S", "iso-8601"],
    #     )
    # )
    # auth_key = serializers.CharField()

    class Meta:
        exclude = [
            "courier_type",
            "tracking_ids",
            "orders_created_at",
            "auth_key",
        ]

    def validate_courier_type(self, data):
        if data not in courier_tracker_map_dict.keys():
            raise serializers.ValidationError(
                "Please enter a valid courier type"
            )
        return data

    def validate_auth_key(self, data):
        if data != settings.AUTH_KEY:
            raise serializers.ValidationError("Please enter a valid auth key")
        return data

    def validate(self, data):
        tracking_ids = data.get("tracking_ids")
        # orders_created_at = data.get("orders_created_at")
        # if len(tracking_ids) != len(orders_created_at):
        #     raise serializers.ValidationError(
        #         "Please add order created at for all tracking IDs or vice-versa"
        #     )
        return data
