import json
from datetime import datetime

import pytz
import requests
import xmltodict
from django.conf import settings
from sentry_sdk import capture_exception
from pickrr_tracker.loggers import notification_logger

class ComplexEncoder(json.JSONEncoder):
     def default(self, obj):
         try:
             return json.JSONEncoder.default(self, obj)
         except:
             return str(obj)
         
         
def xml_to_dict(data):
    try:
        data = xmltodict.parse(data, attr_prefix="", cdata_key="value")
    except Exception as e:
        # capture_exception(e)
        return {"is_success": False, "msg": str(e), "data": None}
    else:
        data = json.dumps(data)
        return {
            "is_success": True,
            "msg": None,
            "data": {"courier_response": data},
        }


class GETRequest:
    def __init__(
        self,
        url,
        params=None,
        xml=False,
        timeout=settings.DEFAULT_REQUESTS_TIMEOUT,
        headers=None,
        return_xml=False,
    ):
        self.url = url
        self.params = params
        self.xml = xml
        self.timeout = timeout
        self.headers = headers
        self.return_xml = return_xml

    @staticmethod
    def response_to_json(response):
        try:
            data = response.json()
        except Exception as e:
            return {"is_success": False, "msg": str(e), "data": None}
        else:
            return {
                "is_success": True,
                "msg": None,
                "data": {"courier_response": data},
            }

    def send(self):
        try:
            r = requests.get(
                self.url,
                params=self.params,
                timeout=self.timeout,
                headers=self.headers,
            )
        except requests.exceptions.RequestException as e:
            # capture_exception(e)
            return {"is_success": False, "msg": str(e), "data": None}
        else:
            if self.xml and not self.return_xml:
                return xml_to_dict(r.text)
            if self.xml and self.return_xml:
                return {
                    "is_success": True,
                    "msg": None,
                    "data": {"courier_response": r.text},
                }
            return self.response_to_json(r)


class POSTRequest:
    def __init__(
        self,
        url,
        data=None,
        xml=False,
        timeout=settings.DEFAULT_REQUESTS_TIMEOUT,
        headers=None,
        json_dumps=True,
    ):
        self.url = url
        self.data = data
        self.xml = xml
        self.timeout = timeout
        self.headers = headers
        self.json_dumps = json_dumps

    @staticmethod
    def response_to_json(response):
        try:
            data = response.json()
        except Exception as e:
            # capture_exception(e)
            return {"is_success": False, "msg": str(e), "data": None}
        else:
            return {
                "is_success": True,
                "msg": None,
                "data": {"courier_response": data},
            }

    def send(self):
        try:
            r = requests.post(
                self.url,
                data=json.dumps(self.data) if self.json_dumps else self.data,
                timeout=self.timeout,
                headers=self.headers,
            )
        except requests.exceptions.RequestException as e:
            # capture_exception(e)
            return {"is_success": False, "msg": str(e), "data": None}
        else:
            if self.xml:
                return xml_to_dict(r.text)
            return self.response_to_json(r)


def dt_str_to_dt_obj(dt_str, dt_format):
    dt_obj = datetime.strptime(dt_str, dt_format)
    dt_obj = pytz.utc.localize(dt_obj)
    return dt_obj


def read_post_json(json_val):
    try:
        return json.loads(json_val)
    except Exception as e:
        return {"err": str(e)}


def split_list_into_batches(list_of_data: list, max_batch_size: int = 10):
    batches = []
    if len(list_of_data):
        start = 0
        chunk_size = max_batch_size
        while start + chunk_size < len(list_of_data):
            batches.append(list_of_data[start : start + chunk_size])
            start = start + chunk_size
        if start < len(list_of_data):
            batches.append(list_of_data[start : len(list_of_data)])
    return batches

class MakeAPICall:
    def __init__(self, url, payload=None, 
                 timeout=settings.DEFAULT_REQUESTS_TIMEOUT,
                 headers={'content-type': 'application/json'}, params=None):
        self.url = url
        self.payload = payload
        self.timeout = timeout
        self.headers = headers
        self.params = params
        

    def post(self, obj={}, type="Post Request Error"):
        try:
            res = requests.post(self.url, data=json.dumps(self.payload), timeout=self.timeout, headers=self.headers, params=self.params)
            return res.json()
        except Exception as error:
            notification_logger.info({
                "err": str(error),
                "type": type,
                "obj_data": json.dumps(obj, cls=ComplexEncoder),
            })
            return False
        
    def get(self, obj={}, type="Get Request Error"):
        try:
            res = requests.get(self.url, headers=self.headers, timeout=self.timeout, params=self.params)
            return res.json()
        except Exception as error:
            notification_logger.info({
                "err": str(error),
                "type": type,
                "obj_data": json.dumps(obj, cls=ComplexEncoder),
            })
            return False
    
    def put(self):
        pass