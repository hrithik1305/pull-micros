from django.conf import settings

from apps.common.utils import GETRequest
from pickrr_tracker.loggers import kerryindev_logger


def tracker(waybills: list) -> dict:
    params = {"awbno": ",".join(waybills)}
    url = "http://123.108.45.7/pickrrwebservice/service.asmx/Get"
    request = GETRequest(url=url, params=params, xml=True)
    response = request.send()
    kerryindev_logger.info(
        {"url": url, "params": params, "courier_response": str(response)}
    )
    return response
