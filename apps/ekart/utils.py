import json

import dateutil.parser
from django.conf import settings

from pickrr_tracker.loggers import ekart_logger


def get_token(courier_type: str) -> dict:
    credentials_map = {
        "ekart": {"token": settings.EKART_TOKEN, "merchant_code": "PKR"},
        "ekart_new": {
            "token": settings.EKART_NEW_TOKEN,
            "merchant_code": "WSP",
        },
        "ekart_new_non_est": {
            "token": settings.EKART_NEW_NON_EST_TOKEN,
            "merchant_code": "WSP",
        },
        "ekart_new_surface": {
            "token": settings.EKART_NEW_SURFACE_TOKEN,
            "merchant_code": "WSP",
        },
    }
    return credentials_map[courier_type]


EKART_MAPPER = {
    "SHIPMENT_CREATED": {
        "courier_remarks": "Shipment created successfully in system.",
        "courier_status": "shipment_created",
        "courier_status_type": "shipment_created",
        "pickrr_code": "OP",
        "pickrr_sub_status_code": "",
        "pickrr_status": "Order Placed",
        "pickrr_sub_status": "",
    },
    "PICKUP_SCHEDULED": {
        "courier_remarks": "Pickup from seller location scheduled and expectancy created at ekl-marketplace",
        "courier_status": "pickup_scheduled",
        "courier_status_type": "pickup_scheduled",
        "pickrr_code": "OM",
        "pickrr_sub_status_code": "",
        "pickrr_status": "Order Manifested",
        "pickrr_sub_status": "",
    },
    "OUT_FOR_PICKUP": {
        "courier_remarks": "Shipment is out for pickup.",
        "courier_status": "out_for_pickup",
        "courier_status_type": "out_for_pickup",
        "pickrr_code": "OFP",
        "pickrr_sub_status_code": "",
        "pickrr_status": "Out for pickup",
        "pickrr_sub_status": "",
    },
    "PICKUP_COMPLETE": {
        "courier_remarks": "Shipment is picked from seller location.",
        "courier_status": "pickup_complete",
        "courier_status_type": "pickup_complete",
        "pickrr_code": "PP",
        "pickrr_sub_status_code": "",
        "pickrr_status": "Order Picked Up",
        "pickrr_sub_status": "",
    },
    "PICKUP_REATTEMPT": {
        "courier_remarks": "Shipment pickup failed, reattempt pickup again.",
        "courier_status": "pickup_reattempt",
        "courier_status_type": "pickup_reattempt",
        "pickrr_code": "PPF",
        "pickrr_sub_status_code": "",
        "pickrr_status": "Pickup Failed",
        "pickrr_sub_status": "",
    },
    "PICKUP_CANCELLED": {
        "courier_remarks": "Shipment pickup cancelled, can be due to cancellation, number of pickup attempts exceeded minimum value, etc.",
        "courier_status": "pickup_cancelled",
        "courier_status_type": "pickup_cancelled",
        "pickrr_code": "OC",
        "pickrr_sub_status_code": "",
        "pickrr_status": "Order Cancelled",
        "pickrr_sub_status": "",
    },
    "IN_TRANSIT": {
        "courier_remarks": "Expected/Recieved at MotherHub. Or in transit to a hub",
        "courier_status": "in_transit",
        "courier_status_type": "in_transit",
        "pickrr_code": "OT",
        "pickrr_sub_status_code": "",
        "pickrr_status": "Order in Transit",
        "pickrr_sub_status": "",
    },
    "OUT_FOR_DELIVERY": {
        "courier_remarks": "Shipment is out for delivery.",
        "courier_status": "out_for_delivery",
        "courier_status_type": "out_for_delivery",
        "pickrr_code": "OO",
        "pickrr_sub_status_code": "",
        "pickrr_status": "Order Out for Delivery",
        "pickrr_sub_status": "",
    },
    "UNDELIVERED": {
        "courier_remarks": "Shipment was undelivered, can be due to- delivery attempted and failed, delivery not attempted, shipment got damaged, heavy load, vehivle breakdown, heavy traffic, heavy rain, door was locked, holiday, etc.",
        "courier_status": "undelivered",
        "courier_status_type": "undelivered",
        "pickrr_code": "UD",
        "pickrr_sub_status_code": "SD",
        "pickrr_status": "Failed Attempt at Delivery",
        "pickrr_sub_status": "Shipper Delay",
    },
    "UNSUCCESSFUL_DELIVERY_ATTEMPT_DUE_TO_ADDRESS_ISSUES": {
        "courier_remarks": "Shipment delivery attempt unsuccessful due to incomplete customer address or provided address not found.",
        "courier_status": "unsuccessful_delivery_attempt_due_to_address_issues",
        "courier_status_type": "unsuccessful_delivery_attempt_due_to_address_issues",
        "pickrr_code": "UD",
        "pickrr_sub_status_code": "AI",
        "pickrr_status": "Failed Attempt at Delivery",
        "pickrr_sub_status": "Address Issue",
    },
    "UNDELIVERED_DUE_TO_REJECTION_BY_CUSTOMER": {
        "courier_remarks": "Shipment delivery failed due to customer rejection.",
        "courier_status": "undelivered_due_to_rejection_by_customer",
        "courier_status_type": "undelivered_due_to_rejection_by_customer",
        "pickrr_code": "UD",
        "pickrr_sub_status_code": "CR",
        "pickrr_status": "Failed Attempt at Delivery",
        "pickrr_sub_status": "Customer Refused Shipment",
    },
    "UNDELIVERED_DUE_TO_CUSTOMER_UNAVAILABILITY": {
        "courier_remarks": "Shipment undelivered due to customer unavailability or delivery reschedule requested by customer.",
        "courier_status": "undelivered_due_to_customer_unavailability",
        "courier_status_type": "undelivered_due_to_customer_unavailability",
        "pickrr_code": "UD",
        "pickrr_sub_status_code": "CNA",
        "pickrr_status": "Failed Attempt at Delivery",
        "pickrr_sub_status": "Customer Not Available/Office/Residence Closed/Consignee phone not reachable",
    },
    "UNDELIVERED_DUE_TO_CASH_UNAVAILABILITY": {
        "courier_remarks": "Shipment undelivered as COD not ready",
        "courier_status": "undelivered_due_to_cash_unavailability",
        "courier_status_type": "undelivered_due_to_cash_unavailability",
        "pickrr_code": "UD",
        "pickrr_sub_status_code": "CNR",
        "pickrr_status": "Failed Attempt at Delivery",
        "pickrr_sub_status": "Cash Not Ready",
    },
    "UNSUCCESSFUL_DELIVERY_ATTEMPT_DUE_TO_SERVICEABILITY_ISSUES": {
        "courier_remarks": "Shipment undelivered due to non serviceable pincode",
        "courier_status": "unsuccessful_delivery_attempt_due_to_serviceability_issues",
        "courier_status_type": "unsuccessful_delivery_attempt_due_to_serviceability_issues",
        "pickrr_code": "UD",
        "pickrr_sub_status_code": "ODA",
        "pickrr_status": "Failed Attempt at Delivery",
        "pickrr_sub_status": "Out of Delivery area",
    },
    "DELIVERED": {
        "courier_remarks": "Shipment delivered",
        "courier_status": "delivered",
        "courier_status_type": "delivered",
        "pickrr_code": "DL",
        "pickrr_sub_status_code": "",
        "pickrr_status": "Order Delivered",
        "pickrr_sub_status": "",
    },
    "LOST": {
        "courier_remarks": "Shipment got lost",
        "courier_status": "lost",
        "courier_status_type": "lost",
        "pickrr_code": "LT",
        "pickrr_sub_status_code": "",
        "pickrr_status": "Shipment Lost",
        "pickrr_sub_status": "",
    },
    "UNTRACEABLE": {
        "courier_remarks": "Shipment untraceable",
        "courier_status": "untraceable",
        "courier_status_type": "untraceable",
        "pickrr_code": "LT",
        "pickrr_sub_status_code": "",
        "pickrr_status": "Shipment Lost",
        "pickrr_sub_status": "",
    },
    "RTO_CREATED": {
        "courier_remarks": "Shipment marked for Return To Origin",
        "courier_status": "rto_created",
        "courier_status_type": "rto_created",
        "pickrr_code": "RTO",
        "pickrr_sub_status_code": "",
        "pickrr_status": "Order Returned Back",
        "pickrr_sub_status": "",
    },
    "RTO_IN_TRANSIT": {
        "courier_remarks": "RTO shipment in transit",
        "courier_status": "rto_in_transit",
        "courier_status_type": "rto_in_transit",
        "pickrr_code": "RTO-OT",
        "pickrr_sub_status_code": "",
        "pickrr_status": "RTO in Transit",
        "pickrr_sub_status": "",
    },
    "RTO_COMPLETED": {
        "courier_remarks": "RTO shipment received by merchant",
        "courier_status": "rto_completed",
        "courier_status_type": "rto_completed",
        "pickrr_code": "RTD",
        "pickrr_sub_status_code": "",
        "pickrr_status": "Order Returned to Consignee",
        "pickrr_sub_status": "",
    },
    "RTO_CANCELLED": {
        "courier_remarks": "RTO marked cancelled",
        "courier_status": "rto_cancelled",
        "courier_status_type": "rto_cancelled",
        "pickrr_code": "RTO UD",
        "pickrr_sub_status_code": "",
        "pickrr_status": "RTO Undelivered",
        "pickrr_sub_status": "",
    },
    "DAMAGED": {
        "courier_remarks": "Shipment damaged",
        "courier_status": "damaged",
        "courier_status_type": "damaged",
        "pickrr_code": "DM",
        "pickrr_sub_status_code": "",
        "pickrr_status": "Shipment Damaged",
        "pickrr_sub_status": "",
    },
}


def get_pikrr_response_ekart(waybill_array, response):
    final_tracking_array = []
    from apps.common.services import ist_to_utc_converter

    for waybill in waybill_array:
        tracking_dict = {}
        pickup_datetime = None
        edd_timestamp = None
        if response[waybill] != "Not Found":
            tracking_array = []
            for detail in response[waybill]["history"]:
                track_details = {}
                if detail["status"].upper() in EKART_MAPPER:
                    track_details["scan_type"] = EKART_MAPPER[
                        str(detail["status"]).upper()
                    ]["pickrr_code"]
                else:
                    continue
                if (
                    detail["status"] == "delivered"
                    and detail["cs_notes"] == "Marked_As_RTO"
                ):
                    track_details["scan_type"] = "RTO"
                try:
                    status_time = dateutil.parser.parse(
                        detail["event_date"]
                    ).replace(tzinfo=None)
                    status_time = ist_to_utc_converter(status_time)
                except Exception as e:
                    ekart_logger.info({"err": str(e), "waybill": waybill})
                    from datetime import datetime

                    status_time = datetime.now()
                    status_time = ist_to_utc_converter(status_time)
                track_details["scan_datetime"] = status_time
                track_details["scan_status"] = (
                    detail["public_description"]
                    if detail["public_description"]
                    else ""
                )
                track_details["scan_location"] = detail["city"]
                track_details["pickrr_sub_status_code"] = EKART_MAPPER[
                    str(detail["status"]).upper()
                ]["pickrr_sub_status_code"]
                track_details["courier_status_code"] = str(
                    detail["status"]
                ).upper()
                if track_details["scan_type"] == "PP":
                    pickup_datetime = status_time
                    tracking_dict["pickup_datetime"] = pickup_datetime
                tracking_array.append(track_details.copy())
            if response[waybill].get("weight"):
                tracking_dict["final_weight"] = float(
                    response[waybill]["weight"]
                )
            if len(tracking_array) > 0:
                tracking_dict["awb"] = str(waybill)
                tracking_dict["edd_stamp"] = edd_timestamp
                tracking_dict["received_by"] = ""
                try:
                    if tracking_array[0]["scan_type"] == "DL":
                        tracking_dict["received_by"] = response[waybill][
                            "receiver"
                        ]["name"]
                except Exception as e:
                    ekart_logger.info({"err": str(e), "waybill": waybill})
                    tracking_dict["received_by"] = "SELF"
                tracking_dict["status_date"] = tracking_array[0][
                    "scan_datetime"
                ]
                tracking_dict["status_location"] = tracking_array[0][
                    "scan_location"
                ]
                tracking_dict["status_info"] = tracking_array[0]["scan_status"]
                tracking_dict["status"] = tracking_array[0]["scan_type"]
                if tracking_array[0]["scan_type"] == "NDR":
                    tracking_dict["status_type"] = "OT"
                else:
                    tracking_dict["status_type"] = tracking_array[0][
                        "scan_type"
                    ]
                tracking_dict["tracking_array"] = tracking_array
                final_tracking_array.append(tracking_dict)
        else:
            tracking_dict["err"] = response[waybill]
            tracking_dict["awb"] = str(waybill)
            ekart_logger.error(
                {"err": tracking_dict["err"], "waybill": waybill}
            )
            final_tracking_array.append(tracking_dict)
    return final_tracking_array
