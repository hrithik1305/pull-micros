import json
from datetime import datetime

import requests
from django.conf import settings

from apps.common.utils import POSTRequest
from pickrr_tracker.loggers import xpressbees_logger


def get_token(courier_type: str) -> str:
    credentials_map = {
        "xpressbees": settings.XPRESSBEES_TOKEN,
        "xpressbees_non_est": settings.XPRESSBEES_NON_EST_TOKEN,
        "xpressbees_zookr": settings.XPRESSBEES_ZOOKR_TOKEN,
        "xpressbees_weshyp": settings.XPRESSBEES_WESHYP_TOKEN,
        "xpressbees_reverse": settings.XPRESSBEES_REVERSE_TOKEN,
        "xpressbees_2kg": settings.XPRESSBEES_2KG_TOKEN,
        "xpressbees_5kg": settings.XPRESSBEES_5KG_TOKEN,
        "xpressbees_10kg": settings.XPRESSBEES_10KG_TOKEN,
        "xpressbees_20kg": settings.XPRESSBEES_20KG_TOKEN,
    }
    return credentials_map[courier_type]


XBS_MAPPER = {
    "Data Received-DRC": {"scan_type": "OP", "pickrr_sub_status_code": ""},
    "Pickup Created-PUC": {"scan_type": "OM", "pickrr_sub_status_code": ""},
    "Out for Pickup-OFP": {"scan_type": "OFP", "pickrr_sub_status_code": ""},
    "PickDone-PUD": {"scan_type": "PP", "pickrr_sub_status_code": ""},
    "Attempted but not picked-PND": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "",
    },
    "Order got cancelled-PND": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "CANC",
    },
    "As per vendor no pickup-PND": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SNR",
    },
    "As per vendor pickup not ready today-PND": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SNR",
    },
    "Address Not Found or Address in-complete-PND": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "AI",
    },
    "Not attempt-PND": {"scan_type": "PPF", "pickrr_sub_status_code": "NA"},
    "As per vendor handed over to OneShip-PND": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "HO",
    },
    "As per vendor pickup not ready-PND": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SNR",
    },
    "Concerned person not available-PND": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SU",
    },
    "Vendor shifted from given address-PND": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "AI",
    },
    "As per vendor partial product ready-PND": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SNR",
    },
    "Not contactable-PND": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SU",
    },
    "Shop Closed-PND": {"scan_type": "PPF", "pickrr_sub_status_code": "SC"},
    "ODA Location-PND": {"scan_type": "PPF", "pickrr_sub_status_code": "NA"},
    "Picked-PKD": {"scan_type": "SHP", "pickrr_sub_status_code": ""},
    "InTransit-IT": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "Reached at Destination-RAD": {
        "scan_type": "RAD",
        "pickrr_sub_status_code": "",
    },
    "Out for Delivery-OFD": {"scan_type": "OO", "pickrr_sub_status_code": ""},
    "Residence/Office Closed : Can't Deliver-UD": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNA",
    },
    "Consignee Not Available-UD": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNA",
    },
    "Add Incomplete/ Incorrect & Mobile Not Reachable-UD": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "AI",
    },
    "COD Amount Not Ready-UD": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNR",
    },
    "C'nee shifted On Given Address-UD": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "AI",
    },
    "Future Delivery : Requested by Customer-UD": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CD",
    },
    "Delivery not attempted (time constraint)-UD": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "SD",
    },
    "ODA (Out Of Delivery Area)-UD": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "ODA",
    },
    "Customer Out of Station-UD": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNA",
    },
    "Customer Wants Open Delivery-UD": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "OPDEL",
    },
    "Door Closed-UD": {"scan_type": "UD", "pickrr_sub_status_code": "CNA"},
    "ID Proof Not Provided-UD": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CI",
    },
    "Building Under Construction-UD": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "OTH",
    },
    "Consignee Left The Job or Resigned-UD": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNA",
    },
    "Entry Not Permitted-UD": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "REST",
    },
    "No Such Consignee At The Given Address-UD": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNA",
    },
    "Natural Calamity Cannot Reach-UD": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "SD",
    },
    "Customer Refused to Accept:Currency not available-UD": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CR",
    },
    "Self Collect-UD": {"scan_type": "UD", "pickrr_sub_status_code": "ODA"},
    "Political Disturbance-UD": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "SD",
    },
    "Customer Refused To Accept-UD": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CR",
    },
    "Customer Not Available & Mobile Not Reachable-UD": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNA",
    },
    "Delivered-DLVD": {"scan_type": "DL", "pickrr_sub_status_code": ""},
    "Lost-LOST": {"scan_type": "LT", "pickrr_sub_status_code": ""},
    "RTO Notified-RTON": {"scan_type": "RTO", "pickrr_sub_status_code": ""},
    "Return to Origin-RTO": {"scan_type": "RTO", "pickrr_sub_status_code": ""},
    "Return to Origin Intransit-RTO-IT": {
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "Reached at Origin-RAO": {
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "Return to Origin Out for Delivery-RTO-OFD": {
        "scan_type": "RTO-OO",
        "pickrr_sub_status_code": "",
    },
    "Return Delivered-RTD": {"scan_type": "RTD", "pickrr_sub_status_code": ""},
    "Return Undelivered-RTU": {
        "scan_type": "RTO UD",
        "pickrr_sub_status_code": "",
    },
    "ShipmentTransit damage-STD": {
        "scan_type": "DM",
        "pickrr_sub_status_code": "",
    },
    "Shortage-STG": {"scan_type": "LT", "pickrr_sub_status_code": ""},
    "Return To Origin Shortage-RTO-STG": {
        "scan_type": "LT",
        "pickrr_sub_status_code": "",
    },
}

XBS_REVERSE_MAPPER = {
    "RPPickupPending": {"scan_type": "OM", "pickrr_sub_status_code": ""},
    "RPPickDone": {"scan_type": "PP", "pickrr_sub_status_code": ""},
    "RPOutForPickup": {"scan_type": "OFP", "pickrr_sub_status_code": ""},
    "RPCancel": {"scan_type": "OC", "pickrr_sub_status_code": ""},
    "RPAttemptNotPick_2": {"scan_type": "PPF", "pickrr_sub_status_code": "SU"},
    "RPAttemptNotPick_3": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "DAM",
    },
    "RPAttemptNotPick_4": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SNR",
    },
    "RPAttemptNotPick_5": {"scan_type": "PPF", "pickrr_sub_status_code": "SU"},
    "RPAttemptNotPick_6": {"scan_type": "PPF", "pickrr_sub_status_code": "SU"},
    "RPAttemptNotPick_7": {"scan_type": "PPF", "pickrr_sub_status_code": "HO"},
    "RPAttemptNotPick_8": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "OTH",
    },
    "RPAttemptNotPick_9": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SNR",
    },
    "RPAttemptNotPick_12": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "AI",
    },
    "RPAttemptNotPick_17": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SNR",
    },
    "RPAttemptNotPick_18": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "NA",
    },
    "RPAttemptNotPick_23": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "NA",
    },
    "RPAttemptNotPick_24": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "OTH",
    },
    "RPAttemptNotPick_25": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "CANC",
    },
    "RPAttemptNotPick_26": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "OTH",
    },
    "IT": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "RAD": {"scan_type": "RAD", "pickrr_sub_status_code": ""},
    "OFD": {"scan_type": "OO", "pickrr_sub_status_code": ""},
    "UD_7": {"scan_type": "UD", "pickrr_sub_status_code": "AI"},
    "UD_8": {"scan_type": "UD", "pickrr_sub_status_code": "CNA"},
    "UD_13": {"scan_type": "UD", "pickrr_sub_status_code": "SD"},
    "UD_14": {"scan_type": "UD", "pickrr_sub_status_code": "CD"},
    "UD_16": {"scan_type": "UD", "pickrr_sub_status_code": "SD"},
    "UD_17": {"scan_type": "UD", "pickrr_sub_status_code": "CI"},
    "UD_18": {"scan_type": "UD", "pickrr_sub_status_code": "AI"},
    "UD_19": {"scan_type": "UD", "pickrr_sub_status_code": "SD"},
    "UD_20": {"scan_type": "UD", "pickrr_sub_status_code": "SD"},
    "UD_21": {"scan_type": "UD", "pickrr_sub_status_code": "OPDEL"},
    "DLVD": {"scan_type": "DL", "pickrr_sub_status_code": ""},
}


def get_pkr_response_xbs(response):
    final_tracking_array = []
    pickup_datetime = None
    from apps.common.services import ist_to_utc_converter

    for res in response:
        trackingdict = {}
        edd_stamp = None
        received_by = ""
        tracking_array = []
        if (
            res["AuthKey"] == "Valid"
            and res["ReturnMessage"] == "Successful"
            and res["ShipmentSummary"]
        ):
            for track in res["ShipmentSummary"]:
                xpressbees_dict = {}
                date = track["StatusDate"] + " " + track["StatusTime"]
                xpressbees_dict["scan_datetime"] = datetime.strptime(
                    date, "%d-%m-%Y %H%M"
                )
                xpressbees_dict["scan_datetime"] = ist_to_utc_converter(
                    xpressbees_dict["scan_datetime"]
                )
                xpressbees_dict["scan_location"] = track["Location"]
                xpressbees_dict["scan_status"] = (
                    track["Status"] + " - " + track["Comment"]
                )
                mapper_string = (
                    str(track["Status"]) + "-" + str(track["StatusCode"])
                )
                if mapper_string in XBS_MAPPER:
                    xpressbees_dict["scan_type"] = XBS_MAPPER[
                        str(mapper_string)
                    ]["scan_type"]
                    if xpressbees_dict["scan_type"] == "DL":
                        xpressbees_dict["scan_status"] = track["Status"]
                        comments = track["Comment"].lower()
                        if "receiver name" in comments:
                            received_by = (
                                comments.split("receiver name")[-1]
                                .replace("remarks", "")
                                .replace(":", "")
                                .strip()
                                .title()
                            )
                else:
                    continue
                tracking_array.append(xpressbees_dict.copy())
                if xpressbees_dict["scan_type"] == "PP":
                    pickup_datetime = datetime.strptime(date, "%d-%m-%Y %H%M")
                    pickup_datetime = ist_to_utc_converter(pickup_datetime)
        if tracking_array:
            if tracking_array[0]["scan_type"] == "NDR":
                trackingdict["status_type"] = "OT"
            elif (
                "RT" in tracking_array[0]["scan_type"]
                and tracking_array[0]["scan_type"] != "RTD"
            ):
                trackingdict["status_type"] = "RTO"
            else:
                trackingdict["status_type"] = tracking_array[0]["scan_type"]
            trackingdict["status_location"] = tracking_array[0][
                "scan_location"
            ]
            trackingdict["status_date"] = tracking_array[0]["scan_datetime"]
            trackingdict["pickup_datetime"] = pickup_datetime
            trackingdict["awb"] = str(res["AWBNo"])
            trackingdict["edd_stamp"] = edd_stamp
            trackingdict["received_by"] = received_by
            trackingdict["tracking_array"] = tracking_array
            final_tracking_array.append(trackingdict)
    return final_tracking_array


def xpressbees_reverse_tracker(res, awb):
    trackingdict = {}
    from apps.common.services import ist_to_utc_converter

    try:
        trackingdict["awb"] = str(awb)
        tracking_array = []
        pickup_datetime = None
        edd_stamp = None
        received_by = ""
        if res:
            res = res[0]
            if (
                res["ReturnCode"] == "100"
                and res["ReturnMessage"] == "Successful"
                and res["ShipmentSummary"]
            ):
                for track in res["ShipmentSummary"]:
                    xpressbees_dict = {}
                    date = track["StatusDateTime"]
                    xpressbees_dict["scan_datetime"] = datetime.strptime(
                        date, "%d-%m-%Y %H:%M:%S"
                    )
                    xpressbees_dict["scan_datetime"] = ist_to_utc_converter(
                        xpressbees_dict["scan_datetime"]
                    )
                    xpressbees_dict["scan_location"] = track["Location"]
                    xpressbees_dict["scan_status"] = track["Remarks"]
                    if track["Status"] in XBS_REVERSE_MAPPER:
                        if track["ReasonCode"]:
                            xpressbees_dict["scan_type"] = XBS_REVERSE_MAPPER[
                                track["Status"] + "_" + track["ReasonCode"]
                            ]["scan_type"]
                        else:
                            xpressbees_dict["scan_type"] = XBS_REVERSE_MAPPER[
                                track["Status"]
                            ]["scan_type"]
                    else:
                        continue
                    if xpressbees_dict["scan_type"] == "PP":
                        pickup_datetime = xpressbees_dict["scan_datetime"]
                    if track["ReasonCode"]:
                        xpressbees_dict[
                            "pickrr_sub_status_code"
                        ] = XBS_REVERSE_MAPPER[
                            track["Status"] + "_" + track["ReasonCode"]
                        ][
                            "scan_type"
                        ]
                        xpressbees_dict["courier_status_code"] = (
                            track["Status"] + "_" + track["ReasonCode"]
                        )
                    else:
                        xpressbees_dict[
                            "pickrr_sub_status_code"
                        ] = XBS_REVERSE_MAPPER[track["Status"]]["scan_type"]
                        xpressbees_dict["courier_status_code"] = track[
                            "Status"
                        ]
                    tracking_array.append(xpressbees_dict.copy())
                if tracking_array:
                    if tracking_array[0]["scan_type"] == "NDR":
                        trackingdict["status_type"] = "OT"
                    else:
                        trackingdict["status_type"] = tracking_array[0][
                            "scan_type"
                        ]
                    trackingdict["status_location"] = tracking_array[0][
                        "scan_location"
                    ]
                    trackingdict["status_date"] = tracking_array[0][
                        "scan_datetime"
                    ]
                    trackingdict["pickup_datetime"] = pickup_datetime
                    trackingdict["received_by"] = received_by
                    trackingdict["tracking_array"] = tracking_array
            else:
                trackingdict["awb"] = str(awb)
                trackingdict["err"] = res["ReturnMessage"]
        else:
            trackingdict["err"] = "some exceptional error occured"
    except Exception as e:
        xpressbees_logger.error(
            {"err": str(e), "waybill": awb, "courier_response": str(res)}
        )
        return {"waybill": str(awb), "err": str(e)}
    return trackingdict
