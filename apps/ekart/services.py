from apps.common.utils import POSTRequest
from pickrr_tracker.loggers import ekart_logger

from .utils import get_pikrr_response_ekart, get_token


def tracker(courier_type: str, waybills: list) -> dict:
    token_dict = get_token(courier_type)
    headers = {
        "content-type": "application/json",
        "HTTP_X_MERCHANT_CODE": token_dict["merchant_code"],
        "Authorization": "Basic {}".format(token_dict["token"]),
    }
    data = {"tracking_ids": waybills}
    url = "https://api.ekartlogistics.com/v2/shipments/track"
    request = POSTRequest(url=url, data=data, headers=headers)
    response = request.send()
    ekart_logger.info(
        {"url": url, "data": data, "courier_response": str(response)}
    )
    try:
        if response["is_success"]:
            try:
                response["data"]["pickrr_response"] = get_pikrr_response_ekart(
                    waybills, response["data"]["courier_response"]
                )
            except Exception as e:
                ekart_logger.error({"err": str(e)})
                response["data"]["pickrr_response"] = {"err": e}
        return response
    except Exception as e:
        ekart_logger.info({"err": str(e), "awbs": waybills})
