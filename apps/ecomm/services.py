import json
from datetime import datetime

from bs4 import BeautifulSoup
from django.conf import settings

from apps.common.utils import GETRequest
from pickrr_tracker.loggers import ecomm_logger

from .utils import ECOMM_MAPPER


def get_pikrr_response_ecomm(
    courier_type, response, rto_flag, forward_tracking_history
):
    final_tracking_array = []
    re = BeautifulSoup(response, "lxml")
    data = re.body
    data = data.findAll("object", {"model": "awb"})
    from apps.common.services import ist_to_utc_converter

    for shipment in data:
        try:
            trackingdict = {}
            tracking_array = []
            if forward_tracking_history:
                tracking_array += forward_tracking_history
            scans = shipment.find("field", {"name": "scans"})
            scans = scans.findAll("object")
            pickup_datetime = None
            try:
                final_weight_temp = shipment.find(
                    "field", {"name": "actual_weight"}
                )
                trackingdict["final_weight"] = float(
                    final_weight_temp.text.strip()
                )
            except Exception as e:
                ecomm_logger.info({"err": str(e)})
                pass
            for scan in scans:
                ecomm_dict = {}
                reason_code_number = scan.find(
                    "field", {"name": "reason_code_number"}
                ).text.strip()
                if str(reason_code_number) in ECOMM_MAPPER:
                    scan_type = ECOMM_MAPPER[str(reason_code_number)][
                        "pickrr_status_code"
                    ]
                    if rto_flag is True:
                        if scan_type == "DL":
                            scan_type = "RTD"
                        elif scan_type == "OO":
                            scan_type = "RTO-OO"
                        else:
                            scan_type = "RTO-OT"
                    ecomm_dict["scan_type"] = scan_type
                else:
                    continue
                ecommdate = scan.find(
                    "field", {"name": "updated_on"}
                ).text.strip()
                ecommdate = ecommdate.replace("hrs", "").rstrip()
                ecomm_dict["scan_datetime"] = datetime.strptime(
                    ecommdate, "%d %b, %Y, %H:%M"
                )
                ecomm_dict["scan_datetime"] = ist_to_utc_converter(
                    ecomm_dict["scan_datetime"]
                )

                ecomm_dict["scan_location"] = scan.find(
                    "field", {"name": "city_name"}
                ).text.strip()
                ecomm_dict["scan_status"] = (
                    scan.find("field", {"name": "status"}).text.strip()
                    + " "
                    + scan.find("field", {"name": "reason_code"})
                    .text.strip()
                    .replace("-", "")
                    .strip()
                )
                ecomm_dict["pickrr_sub_status_code"] = ECOMM_MAPPER[
                    str(reason_code_number)
                ]["pickrr_sub_status_code"]
                ecomm_dict["courier_status_code"] = str(reason_code_number)
                if ecomm_dict["scan_type"] == "PP":
                    pickup_datetime = ecomm_dict["scan_datetime"]
                tracking_array.append(ecomm_dict.copy())

            if rto_flag is True:
                return tracking_array

            return_awb = str(
                shipment.find("field", {"name": "ref_awb"}).text.strip()
            )

            trackingdict["return_waybill"] = str(
                return_awb if return_awb != "None" else ""
            )
            edd_stamp = None
            try:
                dt = shipment.find("field", {"name": "expected_date"})
                dt = str(dt.text.strip())
                edd_stamp = datetime.strptime(dt, "%d-%b-%Y")
                edd_stamp = ist_to_utc_converter(edd_stamp)
            except Exception as e:
                edd_stamp = None
                ecomm_logger.info(
                    {
                        "err": str(e),
                        "waybill": shipment.find(
                            "field", {"name": "awb_number"}
                        ).text.strip(),
                    }
                )
                pass
            trackingdict["edd_stamp"] = edd_stamp
            if trackingdict["return_waybill"] and not rto_flag:
                return_waybill = [return_awb]
                tracking_array = tracker(
                    courier_type, return_waybill, True, tracking_array
                )
            if tracking_array:
                tracking_array = sorted(
                    tracking_array,
                    key=lambda k: k["scan_datetime"],
                    reverse=True,
                )
                if tracking_array[0]["scan_type"] == "NDR":
                    trackingdict["status_type"] = "OT"
                else:
                    trackingdict["status_type"] = tracking_array[0][
                        "scan_type"
                    ]
                trackingdict["status_location"] = tracking_array[0][
                    "scan_location"
                ]
                trackingdict["status"] = tracking_array[0]["scan_status"]
                trackingdict["status_date"] = tracking_array[0][
                    "scan_datetime"
                ]
            trackingdict["tracking_array"] = tracking_array
            trackingdict["awb"] = str(
                shipment.find("field", {"name": "awb_number"}).text.strip()
            )
            trackingdict["pickup_datetime"] = pickup_datetime
            trackingdict["received_by"] = str(
                shipment.find("field", {"name": "receiver"}).text.strip()
            )
            final_tracking_array.append(trackingdict)
        except Exception as e:
            waybill = str(
                shipment.find("field", {"name": "awb_number"}).text.strip()
            )
            ecomm_logger.error(
                {"err": str(e), "waybill": waybill, "shipment": shipment}
            )
            final_tracking_array.append(
                {"tracking_id": waybill, "err": str(e)}
            )

    return final_tracking_array


def tracker(
    courier_type, waybills: list, rto_flag=False, forward_tracking_history=None
) -> dict:
    params = {
        "username": settings.ECOMM_USERNAME,
        "password": settings.ECOMM_PASSWORD,
        "awb": ",".join(waybills),
    }
    url = "https://plapi.ecomexpress.in/track_me/api/mawbd/"
    request = GETRequest(url=url, params=params, xml=True, return_xml=True)
    response = request.send()
    ecomm_logger.info(
        {"url": url, "params": params, "courier_response": str(response)}
    )
    try:
        if response["is_success"]:
            if rto_flag:
                return get_pikrr_response_ecomm(
                    courier_type,
                    response["data"]["courier_response"],
                    rto_flag,
                    forward_tracking_history,
                )
            else:
                response["data"]["pickrr_response"] = get_pikrr_response_ecomm(
                    courier_type,
                    response["data"]["courier_response"],
                    rto_flag,
                    forward_tracking_history,
                )
        return response
    except Exception as e:
        ecomm_logger.info({"courier_response": str(response), "err": str(e)})
