import json

import boto3
from botocore.client import Config


def send_data_to_event_bridge(
    data_to_send_dict: dict,
    aws_account_region_name: str,
    aws_account_access_key: str,
    aws_account_secret_access_key: str,
    event_bus_source: str,
    event_bus_detail_type: str,
    event_bus_name: str,
):
    data_to_send_str = json.dumps(data_to_send_dict)
    config_eb = Config(connect_timeout=5, retries={"max_attempts": 1})
    client = boto3.client(
        "events",
        region_name=aws_account_region_name,
        aws_access_key_id=aws_account_access_key,
        aws_secret_access_key=aws_account_secret_access_key,
        config=config_eb,
    )
    response = client.put_events(
        Entries=[
            {
                "Source": event_bus_source,
                "DetailType": event_bus_detail_type,
                "Detail": data_to_send_str,
                "EventBusName": event_bus_name,
            }
        ]
    )

    return {"eventbrige_resp": str(response)}
