from graphene.types.generic import GenericScalar
import graphene
from datetime import timedelta, datetime
import pytz
from django.conf import settings

from .preparator import NotificationLogsQuery
from .dao import  NotificationDao
from apps.common.mongo_utils import (
    batch_documents_with_agg, get_db_collection    
)

class NotificationUserData(graphene.ObjectType):
    _id = graphene.String()
    order_placed =  GenericScalar()
    order_picked_up =  GenericScalar()
    picked_up_delayed =  GenericScalar()
    order_in_transit =  GenericScalar()
    delivery_delay =  GenericScalar()
    out_for_delivery_sd =  GenericScalar()
    out_for_delivery_er =  GenericScalar()
    order_delivered =  GenericScalar()
    order_not_delivered =  GenericScalar()
    order_cancelled =  GenericScalar()
    delay_delivery_in_days =  graphene.Int()
    total_coins =  graphene.Float()
    sms_coins =  graphene.Float()
    wp_coins =  graphene.Float()
    email_coins =  graphene.Float()
    free_sms =  graphene.Float()
    free_wp =  graphene.Float()
    free_email =  graphene.Float()
    kwargs =  GenericScalar()

class NotificationCountsData(graphene.ObjectType):
    sms = graphene.Int()
    wp = graphene.Int()
    avg_coins_consumed = graphene.Float()

class NotificationLogsData(graphene.ObjectType):
    _id:graphene.String()
    tracking_id = graphene.String()
    order_id = graphene.String()
    contact_number = graphene.String()
    message_id = graphene.String()
    notification_channel = graphene.String()
    event_type = graphene.String()
    delivery_status = graphene.String()
    consumed_coins = graphene.Float()
    attempted_at = graphene.DateTime()
    remarks = graphene.String()
    kwargs = GenericScalar()


class LogsDataReturnType(graphene.ObjectType):
    total_pages = graphene.Int()
    per_page = graphene.Int()
    data = graphene.List(NotificationLogsData)
    
class NotificationUsersReturnType(graphene.ObjectType):
    counts_data = graphene.Field(NotificationCountsData)
    data = graphene.Field(NotificationUserData)

class Query(graphene.ObjectType):
    notification_users = graphene.Field(
        NotificationUsersReturnType
    )
    notification_logs = graphene.Field(
        LogsDataReturnType,
        page = graphene.Int(),
        is_search = graphene.Boolean(required=True),
        search_key = graphene.String(),
        search_value = graphene.String(),
        event_key = graphene.String(),
        medium_key = graphene.String(),
        from_date = graphene.String(),
        to_date = graphene.String()
    )
    
    def resolve_notification_users(root, info):
        user_email = info.context.META.get('HTTP_X_EMAIL')
        
        filters = {
            "email": user_email,
            "is_active": True
        }
        
        logs_filters_fr_channel_count = [
            {
                '$project': {
                    'attempted_at': 1, 
                    'notification_channel': 1, 
                    'email': 1, 
                    'is_active': 1, 
                    'month': {
                        '$month': '$attempted_at'
                    }
                }
            }, {
                '$match': {
                    'month': datetime.now().month, 
                    'email': user_email, 
                    'is_active': True
                }
            }, {
                '$group': {
                    '_id': '$notification_channel', 
                    'count': {
                        '$sum': 1
                    }
                }
            }
        ]
        
        logs_filter_fr_avg_coins = [
            {
                '$project': {
                    'order_id': 1, 
                    'consumed_coins': 1, 
                    'email': 1, 
                    'is_active': 1, 
                    'month': {
                        '$month': '$attempted_at'
                    }
                }
            }, {
                '$match': {
                    'month': datetime.now().month,
                    'email': user_email,
                    'is_active': True
                }
            }, {
                '$group': {
                    '_id': 'consumed_coins', 
                    'avg_coins': {
                        '$avg': '$consumed_coins'
                    }
                }
            }
        ]
                
        user_data = NotificationDao.get_notification_user(filters)
        chnanel_logs_cursor = batch_documents_with_agg(logs_filters_fr_channel_count, settings.NOTIFICATION_LOGS_COLLECTION_NAME)
        notification_logs_count = {item['_id']: item['count'] for item in chnanel_logs_cursor}
        
        avg_coins_cursor = batch_documents_with_agg(logs_filter_fr_avg_coins, settings.NOTIFICATION_LOGS_COLLECTION_NAME)
        avg_coins_cursor = list(avg_coins_cursor)
        
        counts_data = {}
        counts_data.update(notification_logs_count)
        if avg_coins_cursor:
            counts_data.update({
                "avg_coins_consumed": round(avg_coins_cursor[0].get('avg_coins', 0), 2)
            })

        return {
            "counts_data": counts_data,
            "data": user_data
        }
    
    
    def resolve_notification_logs(root, info, **kwargs):
        PER_PAGE = 10
        user_email = info.context.META.get('HTTP_X_EMAIL')
        tz = pytz.timezone("UTC")
        start_date = datetime.now() - timedelta(days=10)
        start_date = start_date.replace(hour=18, minute=30)
        start_date = datetime.fromtimestamp(start_date.timestamp(), tz)
        filter_kwargs = {
            "email": user_email,
            "is_active": True,
            "attempted_at": {
                "$gte": start_date
            }
        }

        if kwargs and kwargs.get('is_search'):
            NotificationLogsQuery.get_filter_args(filter_kwargs, **kwargs)
            notification_logs_obj =  NotificationDao.get_notification_logs(filter_kwargs, type='bulk')
            logs_data = []
            for log in notification_logs_obj:
                logs_data += log
            return {
                "data": logs_data
            }
        else:
            page = int(kwargs.get('page') or 1)
            filters = {
                'is_active': True,
                'email': user_email, 
                'attempted_at':{
                    '$gte': start_date
                }
            }
            
            logs_collection = get_db_collection(settings.NOTIFICATION_LOGS_COLLECTION_NAME)
            logs_cursor = logs_collection.find(filters).skip(PER_PAGE * (page - 1)).limit(PER_PAGE)
            total_pages = (logs_cursor.count() // PER_PAGE) + 1
            logs_data = [log_data for log_data in logs_cursor]
                    
            return {
                "total_pages": total_pages,
                "per_page": PER_PAGE,
                "data": logs_data
            }

