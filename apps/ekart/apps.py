from django.apps import AppConfig


class EkartConfig(AppConfig):
    name = "apps.ekart"
