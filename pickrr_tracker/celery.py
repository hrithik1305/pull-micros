import os
from datetime import timedelta
import datetime
import pytz

from celery import Celery
from celery.schedules import crontab

# Set the default Django settings module for the 'celery' program.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "pickrr_tracker.settings")

app = Celery("pickrr_tracker")

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object("django.conf:settings", namespace="CELERY")

# Load task modules from all registered Django apps.
app.autodiscover_tasks()

def timezone_IST():
    return datetime.datetime.now(pytz.timezone('Asia/Calcutta'))

app.conf.beat_schedule = {
    "apps.common.tasks.pull_updates_from_courier_cronjob": {
        "task": "apps.common.tasks.pull_updates_from_courier_cronjob",
        "schedule": timedelta(hours=3),
    },
    "apps.common.tasks.pull_updates_from_courier_without_push_event_cronjob": {
        "task": "apps.common.tasks.pull_updates_from_courier_without_push_event_cronjob",
        "schedule": timedelta(hours=1),
    },
    "apps.common.tasks.pull_updates_for_delivered_return_events": {
        "task": "apps.common.tasks.pull_updates_for_delivered_return_events",
        "schedule": timedelta(hours=24),
    },
    'apps.notification.tasks.delay_delivery_task':  {
        'task':'apps.notification.tasks.delay_delivery_task',
        'schedule': crontab(minute=30, hour=18, nowfun=timezone_IST),
    },
}
