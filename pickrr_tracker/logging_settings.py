import datetime
import json
import logging


class CustomFormatter(logging.Formatter):
    def format(self, record):
        try:
            d = record.msg
            if not isinstance(d, dict):
                return str(d)
            result = dict()
            result["message"] = d
            result["name"] = record.name
            result["timestamp"] = datetime.datetime.now().isoformat()
            result["loglevel"] = record.levelname
            result1 = dict()
            result1["pslogs"] = result
            return json.dumps(result1)
        except Exception as e:
            return "Err:{}".format(e)


logging_config = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {
        "require_debug_false": {"()": "django.utils.log.RequireDebugFalse"}
    },
    "formatters": {
        "verbose": {
            "format": "%(levelname)s %(process)d %(asctime)s %(module)s %(filename)s:%(funcName)s():%(lineno)d %(message)s"
        },
        "simple": {"format": "%(levelname)s %(message)s"},
        "courier_log": {
            "()": CustomFormatter,
        },
    },
    "handlers": {
        "default": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": "/tmp/pullserver/pickrr_django.log",
            "maxBytes": 1024 * 1024 * 15,  # 15 MB
            "backupCount": 5,
            "formatter": "verbose",
        },
        "delhivery_handler": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": "/tmp/pullserver/delhivery.log",
            "maxBytes": 1024 * 1024 * 5,  # 5 MB
            "backupCount": 5,
            "formatter": "courier_log",
        },
        "amaze_handler": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": "/tmp/pullserver/amaze.log",
            "maxBytes": 1024 * 1024 * 5,  # 5 MB
            "backupCount": 5,
            "formatter": "courier_log",
        },
        "bluedart_handler": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": "/tmp/pullserver/bluedart.log",
            "maxBytes": 1024 * 1024 * 5,  # 5 MB
            "backupCount": 5,
            "formatter": "courier_log",
        },
        "dtdc_handler": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": "/tmp/pullserver/dtdc.log",
            "maxBytes": 1024 * 1024 * 5,  # 5 MB
            "backupCount": 5,
            "formatter": "courier_log",
        },
        "ecomm_handler": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": "/tmp/pullserver/ecomm.log",
            "maxBytes": 1024 * 1024 * 5,  # 5 MB
            "backupCount": 5,
            "formatter": "courier_log",
        },
        "ekart_handler": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": "/tmp/pullserver/ekart.log",
            "maxBytes": 1024 * 1024 * 5,  # 5 MB
            "backupCount": 5,
            "formatter": "courier_log",
        },
        "fedex_handler": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": "/tmp/pullserver/fedex.log",
            "maxBytes": 1024 * 1024 * 5,  # 5 MB
            "backupCount": 5,
            "formatter": "courier_log",
        },
        "kerryindev_handler": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": "/tmp/pullserver/kerryindev.log",
            "maxBytes": 1024 * 1024 * 5,  # 5 MB
            "backupCount": 5,
            "formatter": "courier_log",
        },
        "parceldo_handler": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": "/tmp/pullserver/parceldo.log",
            "maxBytes": 1024 * 1024 * 5,  # 5 MB
            "backupCount": 5,
            "formatter": "courier_log",
        },
        "shadowfax_handler": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": "/tmp/pullserver/shadowfax.log",
            "maxBytes": 1024 * 1024 * 5,  # 5 MB
            "backupCount": 5,
            "formatter": "courier_log",
        },
        "udaan_handler": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": "/tmp/pullserver/udaan.log",
            "maxBytes": 1024 * 1024 * 5,  # 5 MB
            "backupCount": 5,
            "formatter": "courier_log",
        },
        "xpressbees_handler": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": "/tmp/pullserver/xpressbees.log",
            "maxBytes": 1024 * 1024 * 5,  # 5 MB
            "backupCount": 5,
            "formatter": "courier_log",
        },
        "pushevents_handler": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": "/tmp/pullserver/pushevents.log",
            "maxBytes": 1024 * 1024 * 5,  # 5 MB
            "backupCount": 5,
            "formatter": "courier_log",
        },
        "responseupdate_handler": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": "/tmp/pullserver/responseupdate.log",
            "maxBytes": 1024 * 1024 * 5,  # 5 MB
            "backupCount": 5,
            "formatter": "courier_log",
        },
        "pulldataeventbridge_handler": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": "/tmp/pullserver/pulldataeventbridge.log",
            "maxBytes": 1024 * 1024 * 5,  # 5 MB
            "backupCount": 5,
            "formatter": "courier_log",
        },
        "trackingsync_handler": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": "/tmp/pullserver/trackingsync.log",
            "maxBytes": 1024 * 1024 * 5,  # 5 MB
            "backupCount": 5,
            "formatter": "courier_log",
        },
        "notification_handler": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": "/tmp/pullserver/notification.log",
            "maxBytes": 1024 * 1024 * 5,  # 5 MB
            "backupCount": 5,
            "formatter": "courier_log",
        },
    },
    "loggers": {
        "": {"handlers": ["default"], "level": "DEBUG", "propagate": True},
        "delhivery": {
            "handlers": ["delhivery_handler"],
            "level": "DEBUG",
            "propagate": True,
        },
        "amaze": {
            "handlers": ["amaze_handler"],
            "level": "DEBUG",
            "propagate": True,
        },
        "bluedart": {
            "handlers": ["bluedart_handler"],
            "level": "DEBUG",
            "propagate": True,
        },
        "dtdc": {
            "handlers": ["dtdc_handler"],
            "level": "DEBUG",
            "propagate": True,
        },
        "ecomm": {
            "handlers": ["ecomm_handler"],
            "level": "DEBUG",
            "propagate": True,
        },
        "ekart": {
            "handlers": ["ekart_handler"],
            "level": "DEBUG",
            "propagate": True,
        },
        "fedex": {
            "handlers": ["fedex_handler"],
            "level": "DEBUG",
            "propagate": True,
        },
        "kerryindev": {
            "handlers": ["kerryindev_handler"],
            "level": "DEBUG",
            "propagate": True,
        },
        "parceldo": {
            "handlers": ["parceldo_handler"],
            "level": "DEBUG",
            "propagate": True,
        },
        "shadowfax": {
            "handlers": ["shadowfax_handler"],
            "level": "DEBUG",
            "propagate": True,
        },
        "udaan": {
            "handlers": ["udaan_handler"],
            "level": "DEBUG",
            "propagate": True,
        },
        "xpressbees": {
            "handlers": ["xpressbees_handler"],
            "level": "DEBUG",
            "propagate": True,
        },
        "pushevents": {
            "handlers": ["pushevents_handler"],
            "level": "DEBUG",
            "propagate": True,
        },
        "responseupdate": {
            "handlers": ["responseupdate_handler"],
            "level": "DEBUG",
            "propagate": True,
        },
        "pulldataeventbridge": {
            "handlers": ["pulldataeventbridge_handler"],
            "level": "DEBUG",
            "propagate": True,
        },
        "trackingsync": {
            "handlers": ["trackingsync_handler"],
            "level": "DEBUG",
            "propagate": True,
        },
        "notification": {
            "handlers": ["notification_handler"],
            "level": "DEBUG",
            "propagate": True,
        },
    },
}
