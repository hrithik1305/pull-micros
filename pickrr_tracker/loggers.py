import logging

from .logging_settings import logging_config

delhivery_logger = logging.getLogger("delhivery")
amaze_logger = logging.getLogger("amaze")
bluedart_logger = logging.getLogger("bluedart")
dtdc_logger = logging.getLogger("dtdc")
ecomm_logger = logging.getLogger("ecomm")
ekart_logger = logging.getLogger("ekart")
fedex_logger = logging.getLogger("fedex")
kerryindev_logger = logging.getLogger("kerryindev")
parceldo_logger = logging.getLogger("parceldo")
shadowfax_logger = logging.getLogger("shadowfax")
udaan_logger = logging.getLogger("udaan")
xpressbees_logger = logging.getLogger("xpressbees")
pushevents_logger = logging.getLogger("pushevents")
responseupdate_logger = logging.getLogger("responseupdate")
pulldataeventbridge_logger = logging.getLogger("pulldataeventbridge")
trackingsync_logger = logging.getLogger("trackingsync")
notification_logger = logging.getLogger("notification")
