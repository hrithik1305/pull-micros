from functools import reduce
from dateutil.parser import parse
import json
from datetime import datetime
class PrepareTrackingEventsData:
    
    @staticmethod
    def get_current_status_obj(status):
        current_status =  status['current_status_type']
        status_body = status['current_status_body']
        status_time = status['current_status_time']
        return current_status, status_body, status_time
   
    
    @staticmethod
    def get_shorter_product_name(product_name):
        return product_name[:12] + "..."


    @classmethod
    def get_product_name(cls, data):
        if not data.get('item_list'):
            return ''

        item_name_list = []
        item_list = data.get('item_list')

        for item in item_list:
            item_name_list.append(item['item_name'])
            
        item_name = ", ".join(item_name_list)
        item_name = item_name or data.get('product_name')
        shorted_name = cls.get_shorter_product_name(item_name)
        
        return shorted_name


    @classmethod
    def get_events_info_data(cls, events, event_obj):
        info = events['info']
        event_obj['to_email'] = info.get('to_email')
        event_obj['to_number'] = info.get('to_phone_number')
        event_obj['to_name'] = info.get('to_name') or "Customer"
        event_obj['courier_used'] = events.get('courier_parent_name') or events.get('courier_used')
        event_obj['company_name'] = events.get('company_name') or ''
        try:
            event_obj['edd_stamp'] = parse(events.get('edd_stamp')).strftime("%Y-%m-%d") if events.get('edd_stamp') else 'Soon'
        except:
            event_obj['edd_stamp'] = events.get('edd_stamp')
        event_obj['product_name'] = cls.get_product_name(events)
        current_status, status_body, _ = cls.get_current_status_obj(events['status'])
        
        event_obj['current_status'] = current_status
        event_obj['status_body'] = status_body
        event_obj['payment'] = 'COD' if events.get('is_cod') else 'PREPAID'
        event_obj['item_list'] = events.get('item_list') or []
        event_obj['total_price'] = reduce(lambda x, y: x + (y['price'] or 0), event_obj['item_list'], 0) #TODO: confirm
        event_obj['from_name'] = info.get('from_name') or "Seller"
        event_obj['to_address'] = info.get('to_address')
        event_obj['to_city'] = info.get('to_city')
        event_obj['to_state'] = info.get('to_state')
        event_obj['to_pincode'] = info.get('to_pincode')
        event_obj['to_phone_number'] = info.get('to_phone_number')
        event_obj['from_phone_number'] = info.get('from_phone_number')


    @classmethod
    def prepare_events_data(cls, tracking_obj):
        event_obj = {}
        event_obj['tracking_id'] = tracking_obj['tracking_id']
        event_obj['order_id'] = tracking_obj.get('client_order_id') #TODO: confirm ??
        cls.get_events_info_data(tracking_obj, event_obj)
        return event_obj


    @classmethod
    def prepare_events_data_from_cache(cls, events_obj, tracking_id):
        event_obj = {}
        event_obj['tracking_id'] = tracking_id
        event_obj['order_id'] = events_obj.get('client_order_id')
        cls.get_events_info_data(events_obj, event_obj)
        return event_obj


class GetSMSTemplates:

    @staticmethod
    def order_placed(obj):
        return f"Confirmed. Your order with ID {obj.get('order_id')} for {obj.get('product_name')} from {obj.get('company_name')} is placed. Track {obj.get('tracking_url')} Powered by Pickrr."
    

    @staticmethod
    def order_picked_up(obj):
        return f"Picked. Your order with ID {obj.get('order_id')} from {obj.get('company_name')} has been picked. Track here {obj.get('tracking_url')} Powered by Pickrr."


    @staticmethod
    def picked_up_delayed(obj):
        return f"Delayed. Your order with ID {obj.get('order_id')} from {obj.get('company_name')} could not be picked up. We will notify you when it's picked up. Track here {obj.get('tracking_url')} Powered by Pickrr."

    @staticmethod
    def order_in_transit(obj):
        return f"In Transit. Your order with ID {obj.get('order_id')} from {obj.get('company_name')} is in transit. It will be delivered by {obj.get('edd_stamp')}. Track here {obj.get('tracking_url')} Powered by Pickrr."


    @staticmethod
    def delivery_delay(obj):
        return f"Delayed. Your Order with ID {obj.get('order_id')} from {obj.get('company_name')} has been delayed and is expected to be delivered by {obj.get('edd_stamp')}. We apologize for the delay. Track here {obj.get('tracking_url')} \\n Powered by Pickrr."


    @staticmethod
    def out_for_delivery_sd(obj):
        return f"Arriving today. Your order with ID {obj.get('order_id')} from {obj.get('company_name')} is arriving today. Track here {obj.get('tracking_url')} Powered by Pickrr."
    

    @staticmethod
    def out_for_delivery_er(obj):
        return f"Arriving early. Your order with ID {obj.get('order_id')} from {obj.get('company_name')} is arriving today. Track here {obj.get('tracking_url')} Powered by Pickrr."


    @staticmethod
    def order_delivered(obj):
        return f"Delivered. Your order with ID {obj.get('order_id')} from {obj.get('company_name')} has been delivered. Track here {obj.get('tracking_url')} Powered by Pickrr."


    @staticmethod
    def order_not_delivered(obj):
        return f"Not Delivered. Your order {obj.get('order_id')} from {obj.get('company_name')} could not be delivered. To raise a concern comment here {obj.get('tracking_url')} Powered by Pickrr."


    @staticmethod
    def order_cancelled(obj):
        return f"Returned. Your order ID - {obj.get('order_id')} from {obj.get('company_name')} is cancelled as per your request. Track or add comments here {obj.get('tracking_url')} Powered by Pickrr."
    

class GetWPTemplates:

    @staticmethod
    def order_placed(obj):
        params = []
        params.append(obj['to_name'])
        params.append(obj['order_id'])
        params.append(obj['product_name'])
        params.append(obj['company_name'])
        params.append(obj['tracking_id'])
        
        return ",".join(f'"{param}"' for param in params)


    @staticmethod
    def order_picked_up(obj):
        params = []
        params.append(obj['to_name'])
        params.append(obj['order_id'])
        params.append(obj['company_name'])
        params.append(obj['courier_used'])
        params.append(obj['tracking_id'])

        return ",".join(f'"{param}"' for param in params)


    @staticmethod
    def picked_up_delayed(obj):
        params = []
        params.append(obj['to_name'])
        params.append(obj['order_id'])
        params.append(obj['company_name'])
        params.append(obj['tracking_id'])

        return ",".join(f'"{param}"' for param in params)


    @staticmethod
    def order_in_transit(obj):
        params = []
        params.append(obj['to_name'])
        params.append(obj['order_id'])
        params.append(obj['company_name'])
        params.append(obj['courier_used'])
        params.append(obj['edd_stamp'])
        params.append(obj['tracking_id'])

        return ",".join(f'"{param}"' for param in params)


    @staticmethod
    def delivery_delay(obj):
        params = []
        params.append(obj['to_name'])
        params.append(obj['order_id'])
        params.append(obj['company_name'])
        params.append(obj['edd_stamp'])
        params.append(obj['tracking_id'])

        return ",".join(f'"{param}"' for param in params)


    @staticmethod
    def out_for_delivery_sd(obj):
        params = []
        params.append(obj['to_name'])
        params.append(obj['order_id'])
        params.append(obj['company_name'])
        params.append(obj['tracking_id'])

        return ",".join(f'"{param}"' for param in params)


    @staticmethod
    def out_for_delivery_er(obj):
        params = []
        params.append(obj['to_name'])
        params.append(obj['order_id'])
        params.append(obj['company_name'])
        params.append(obj['tracking_id'])

        return ",".join(f'"{param}"' for param in params)


    @staticmethod
    def order_delivered(obj):
        params = []
        params.append(obj['to_name'])
        params.append(obj['order_id'])
        params.append(obj['company_name'])
        params.append(obj['tracking_id'])

        return ",".join(f'"{param}"' for param in params)


    @staticmethod
    def order_not_delivered(obj):
        params = []
        params.append(obj['to_name'])
        params.append(obj['order_id'])
        params.append(obj['company_name'])
        params.append(obj['status_body'])
        params.append(obj['tracking_id'])

        return ",".join(f'"{param}"' for param in params)


    @staticmethod
    def order_cancelled(obj):
        params = []
        params.append(obj['to_name'])
        params.append(obj['order_id'])
        params.append(obj['tracking_id'])
        params.append(obj['company_name'])
        params.append(obj['tracking_id'])

        return ",".join(f'"{param}"' for param in params)


class NotificationLogsQuery:
    """ 
    earch_key: order_id, contact_number, tracking_id
    event_key
    medium_key
    from_date
    to_date
    """
    @classmethod
    def get_filter_args(cls, filter_kwargs, **kwargs):
             
        try:
            for key, value in kwargs.items():
                if key == 'event_key':
                    filter_kwargs['event_type'] = value.upper()
                elif key == 'medium_key':
                    filter_kwargs['notification_channel'] = value
                elif key == 'search_key':
                    filter_kwargs[value] = kwargs.get('search_value') or ''
                elif key == 'from_date':
                    filter_kwargs['attempted_at'].update({
                        "$gte": datetime.strptime(kwargs.get('from_date'), '%Y-%m-%d').replace(hour=18, minute=30),
                        "$lte": datetime.strptime(kwargs.get('to_date'), '%Y-%m-%d').replace(hour=18, minute=29),
                    })
        except:
            raise Exception("Invalid Filter type")
        
        