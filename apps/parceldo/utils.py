import copy
from datetime import datetime

import dateutil.parser

from pickrr_tracker.loggers import parceldo_logger

parceldo_new_code_mapping = {
    "102": {
        "status": "Shipment Pickup in Process",
        "pickrr_status": "Out for pickup",
        "scan_type": "OFP",
        "pickrr_sub_status_code": "",
    },
    "103": {
        "status": "Received at location",
        "pickrr_status": "Shipped",
        "scan_type": "SHP",
        "pickrr_sub_status_code": "",
    },
    "104": {
        "status": "Received at location",
        "pickrr_status": "Shipped",
        "scan_type": "SHP",
        "pickrr_sub_status_code": "",
    },
    "105": {
        "status": "Inbound scan",
        "pickrr_status": "Shipped",
        "scan_type": "SHP",
        "pickrr_sub_status_code": "",
    },
    "106": {
        "status": "Manifest Scan",
        "pickrr_status": "Order in Transit",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "107": {
        "status": "Outbound Scan",
        "pickrr_status": "Order in Transit",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "108": {
        "status": "Inscanned Bags, Awaiting Manifest",
        "pickrr_status": "Order in Transit",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "109": {
        "status": "Outbound Scan",
        "pickrr_status": "Order in Transit",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "110": {
        "status": "Out for Delivery",
        "pickrr_status": "Order Out for Delivery",
        "scan_type": "OO",
        "pickrr_sub_status_code": "",
    },
    "111": {
        "status": "Delivered",
        "pickrr_status": "Order Delivered/Order Returned to Seller",
        "scan_type": "DL/RTD",
        "pickrr_sub_status_code": "",
    },
    "112": {
        "status": "Undelivered package, please contact your local WLA Branch",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "SD",
    },
    "113": {
        "status": "Undelivered package, please contact your local WLA Branch",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "SD",
    },
    "114_DA-ANF": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "AI",
    },
    "114_UD-CNA": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNA",
    },
    "114_DA-IA": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "AI",
    },
    "114_CRAS": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CR",
    },
    "114_ODA": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "ODA",
    },
    "114_DA-DOC": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNA",
    },
    "114_DA-AR": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "REST",
    },
    "114_PH-ST": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "SD",
    },
    "114_CS": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "AI",
    },
    "114_NSP": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CI",
    },
    "114_CNCR": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNA",
    },
    "114_PH-NSS": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "SD",
    },
    "114_CNR": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNR",
    },
    "114_DA-PNRDR": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CD",
    },
    "114_AAIC": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CI",
    },
    "114_CWOD": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "OPDEL",
    },
    "114_MARFC": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CI",
    },
    "114_SELFCOLL": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "ODA",
    },
    "114_NOATT": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "SD",
    },
    "114_MISROU": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "SD",
    },
    "114_POH": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "SD",
    },
    "114_POHCI": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CI",
    },
    "114_CSEN": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "OTH",
    },
    "114_LOST": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "SD",
    },
    "114_DELAY": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "SD",
    },
    "114_DA-RENXTDAY": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "SD",
    },
    "114_DEL-WEA": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "SD",
    },
    "114_CUST_REF": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CR",
    },
    "114_DEVEX": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "SD",
    },
    "114_REFCOD": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNR",
    },
    "114_FUDEL": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CD",
    },
    "114_SIEZE": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "SD",
    },
    "114_IDPROOF": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CI",
    },
    "114_REQDEPT": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CI",
    },
    "114_CONST": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "REST",
    },
    "114_NAMEMIS": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CI",
    },
    "114_LEFTJ": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CI",
    },
    "114_NOPER": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CI",
    },
    "114_NAT": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "OTH",
    },
    "114_DISP": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CI",
    },
    "114_NOND": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CI",
    },
    "114_INCADD": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNA",
    },
    "114_CNAACNNR": {
        "status": "Delivery re-scheduled",
        "pickrr_status": "Failed Attempt at Delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNA",
    },
    "117": {
        "status": "Return To Origin",
        "pickrr_status": "Order Returned Back",
        "scan_type": "RTO",
        "pickrr_sub_status_code": "",
    },
    "121": {
        "status": "Return Delivered",
        "pickrr_status": "Order Returned to Seller",
        "scan_type": "RTD",
        "pickrr_sub_status_code": "",
    },
    "122": {
        "status": "Shipment Received At Facility",
        "pickrr_status": "Order in Transit",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "123": {
        "status": "Shipment Forwarded To Destination",
        "pickrr_status": "Order in Transit",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "124": {
        "status": "Data Received",
        "pickrr_status": "Order Placed",
        "scan_type": "OP",
        "pickrr_sub_status_code": "",
    },
    "125": {
        "status": "Return To Origin Initiated",
        "pickrr_status": "Order Returned Back",
        "scan_type": "RTO",
        "pickrr_sub_status_code": "",
    },
    "126": {
        "status": "Address Change Shipment Redirect",
        "pickrr_status": "Order in Transit",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "127": {
        "status": "Shipment Manifest Generated",
        "pickrr_status": "Order Manifested",
        "scan_type": "OM",
        "pickrr_sub_status_code": "",
    },
    "133": {
        "status": "Shipment Picked",
        "pickrr_status": "Order Picked Up",
        "scan_type": "PP",
        "pickrr_sub_status_code": "",
    },
    "134": {
        "status": "Pickup Initiated",
        "pickrr_status": "Out for pickup",
        "scan_type": "OFP",
        "pickrr_sub_status_code": "",
    },
    "136": {
        "status": "Pickup Completed",
        "pickrr_status": "Out for pickup",
        "scan_type": "OFP",
        "pickrr_sub_status_code": "",
    },
    "137": {
        "status": "Reached at Destination",
        "pickrr_status": "Reached at Destination",
        "scan_type": "RAD",
        "pickrr_sub_status_code": "",
    },
    "138": {
        "status": "Pending Child Order Data",
        "pickrr_status": "Not to be mapped",
        "scan_type": "",
        "pickrr_sub_status_code": "",
    },
    "139": {
        "status": "Reverse Pickup Canceled",
        "pickrr_status": "Not to be mapped",
        "scan_type": "",
        "pickrr_sub_status_code": "",
    },
    "142": {
        "status": "RTO Not Delivered",
        "pickrr_status": "RTO Undelivered",
        "scan_type": "RTO UD",
        "pickrr_sub_status_code": "",
    },
    "143": {
        "status": "Delivery Boy Changed",
        "pickrr_status": "Not to be mapped",
        "scan_type": "",
        "pickrr_sub_status_code": "",
    },
}


def get_parceldo_tracking_details(response, waybill):
    tracking_dict = {}
    from apps.common.services import ist_to_utc_converter

    try:
        if response["status"] == "SUCCESS":
            tracking_array = []
            for detail in response["response"]["packetsHistory"]:
                track_details = {}
                if detail["reasonCode"]:
                    map_key = (
                        str(detail["statusId"])
                        + "_"
                        + str(detail["reasonCode"])
                    )
                else:
                    map_key = str(detail["statusId"])
                if map_key in parceldo_new_code_mapping:
                    track_details["scan_type"] = parceldo_new_code_mapping[
                        map_key
                    ]["scan_type"]
                else:
                    continue
                if str(detail["statusId"]) == "111" and detail["isRTO"] == "N":
                    track_details["scan_type"] = "DL"
                try:
                    status_time = dateutil.parser.parse(
                        detail["date"]
                    ).replace(tzinfo=None)
                    status_time = ist_to_utc_converter(status_time)
                except Exception as e:
                    parceldo_logger.error({"err": str(e), "waybill": waybill})
                    status_time = datetime.now()
                    status_time = ist_to_utc_converter(status_time)
                track_details["scan_datetime"] = status_time
                track_details["scan_status"] = (
                    detail["activity"] if detail["activity"] else ""
                )
                track_details["scan_location"] = detail["location"]
                tracking_dict["pickup_datetime"] = None
                if track_details["scan_type"] == "PP":
                    tracking_dict["pickup_datetime"] = status_time
                track_details[
                    "pickrr_sub_status_code"
                ] = parceldo_new_code_mapping[map_key][
                    "pickrr_sub_status_code"
                ]
                track_details["courier_status_code"] = str(map_key)
                tracking_array.append(track_details.copy())
            tracking_dict["edd_stamp"] = None
            try:
                tracking_dict["edd_stamp"] = dateutil.parser.parse(
                    response["expected_delivery_date"]
                )
                tracking_dict["edd_stamp"] = ist_to_utc_converter(
                    tracking_dict["edd_stamp"]
                )
            except Exception as e:
                parceldo_logger.error({"err": str(e), "waybill": waybill})
                pass
            if len(tracking_array) > 0:
                tracking_dict["received_by"] = ""
                if tracking_array[0]["scan_type"] == "DL":
                    tracking_dict["received_by"] = "SELF"
                tracking_dict["status_date"] = tracking_array[-1][
                    "scan_datetime"
                ]
                tracking_dict["status_location"] = tracking_array[-1][
                    "scan_location"
                ]
                tracking_dict["status_info"] = tracking_array[-1][
                    "scan_status"
                ]
                tracking_dict["status"] = tracking_array[-1]["scan_type"]
                if tracking_array[-1]["scan_type"] == "NDR":
                    tracking_dict["status_type"] = "OT"
                else:
                    tracking_dict["status_type"] = tracking_array[-1][
                        "scan_type"
                    ]
                tracking_dict["tracking_array"] = tracking_array
                tracking_dict["awb"] = str(waybill)
        else:
            tracking_dict["awb"] = str(waybill)
            tracking_dict["err"] = response["message"]

        return tracking_dict
    except Exception as e:
        parceldo_logger.error(
            {
                "err": str(e),
                "waybill": waybill,
                "courier_response": str(response),
            }
        )
        return {"tracking_id": str(waybill), "err": str(e)}
