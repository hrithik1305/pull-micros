import requests

from apps.common.utils import GETRequest
from pickrr_tracker.loggers import udaan_logger

from .utils import get_pikrr_response_udaan, get_token


def tracker(courier_type: str, waybills: list) -> dict:
    token_dict = get_token(courier_type)
    headers = {
        "content-type": "application/json",
        "Authorization": token_dict["token"],
    }
    final_tracking_array = []
    final_courier_response = []
    for waybill in waybills:
        url = (
            "https://udaan.com/api/udaan-express/integration/v1/shipment/tracking/%s"
            % (waybill)
        )
        request = GETRequest(url=url, headers=headers)
        response = request.send()
        udaan_logger.info({"url": url, "courier_response": str(response)})
        final_courier_response.append(response["data"]["courier_response"])
        if response["is_success"]:
            try:
                tracking_dict = get_pikrr_response_udaan(
                    waybill, response["data"]["courier_response"]
                )
            except Exception as e:
                udaan_logger.error({"err": str(e), "waybill": waybill})
                tracking_dict = {"err": e}
            final_tracking_array.append(tracking_dict)
    response["data"]["courier_response"] = final_courier_response
    response["data"]["pickrr_response"] = final_tracking_array
    return response
