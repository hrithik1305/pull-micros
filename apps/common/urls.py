from django.urls import include, path

from apps.common import views
from apps.common.views import TrackPushBulkSyncView, TrackPushSyncView

urlpatterns = [
    path("api/", include("apps.common.api.urls")),
    path(
        "external/push/async-pickrr/",
        TrackPushSyncView.as_view(),
        name="track_push_async_pickrr",
    ),
    path(
        "external/push/async-pickrr/bulk/",
        TrackPushBulkSyncView.as_view(),
        name="track_push_async_pickrr_v2",
    ),
]
