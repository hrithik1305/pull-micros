from django.conf import settings


def get_token_and_url(courier_type: str, awb) -> dict:
    credentials_map = {
        "shadowfax": {
            "token": settings.SHADOWFAX_TOKEN,
            "url": "http://dale.shadowfax.in/api/v3/clients/orders/{}/track/?format=json".format(
                awb
            ),
        },
        "shadowfax_bulk": {
            "token": settings.SHADOWFAX_BULK_TOKEN,
            "url": "http://dale.shadowfax.in/api/v3/clients/orders/{}/track/?format=json".format(
                awb
            ),
        },
        "shadowfax_bulk_reverse": {
            "token": settings.SHADOWFAX_BULK_REVERSE_TOKEN,
            "url": "https://reverse.shadowfax.in/api/v3/clients/requests/{}".format(
                awb
            ),
        },
        "shadowfax_reverse": {
            "token": settings.SHADOWFAX_REVERSE_TOKEN,
            "url": "https://reverse.shadowfax.in/api/v3/clients/requests/{}".format(
                awb
            ),
        },
    }
    return credentials_map[courier_type]
