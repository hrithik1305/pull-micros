from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views
from .schema import schema

urlpatterns = [
    path('', csrf_exempt(views.TokenAuthGraphQLView.as_view(schema=schema))),
    path('webhook/', views.NotificationWebhooks.as_view(), name='notification-webhooks'),
]
