from django.apps import AppConfig


class ShadowfaxConfig(AppConfig):
    name = "apps.shadowfax"
