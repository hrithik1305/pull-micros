from datetime import datetime

from celery import shared_task
from celery.contrib import rdb
from django.http import JsonResponse
from rest_framework import status
from rest_framework.response import Response

from apps.common.services import validate_track_data
from pickrr_tracker.loggers import pushevents_logger

from .models import TrackingInfo
from .services import update_status_db


@shared_task
def send_pulled_data_to_async(data_to_send_dict: dict):
    from .services import send_pulled_data_to_async_via_event_bridge

    return send_pulled_data_to_async_via_event_bridge(data_to_send_dict)


@shared_task
def tracking_sync_task(params):
    from .services import tracking_sync

    res = tracking_sync(params)
    return res


@shared_task
def report_mongo_sync_pull_mongo_task(params):
    from .services import report_data_sync_pull

    res = report_data_sync_pull(params)
    return res


@shared_task
def track_pull_update_webhook_async(track_data):
    err_list = []
    try:
        is_update = False
        if not ("EDD" in track_data and track_data["EDD"]):
            track_data["EDD"] = ""
        if not ("scan_datetime" in track_data and track_data["scan_datetime"]):
            track_data["scan_datetime"] = ""

        validations = validate_track_data(track_data)
        if "err" in validations:
            pushevents_logger.error(
                {"err": validations, "awb": str(track_data["awb"])}
            )
            return {"success": False}
        # if (("scan_type" in track_data)and track_data["scan_type"]and (track_data["scan_type"] in ["PP","CC"])):
        # return {"success": True}
        from decouple import config

        from .dao import get_tracking_info_doc_using_tracking_id

        col_name = config("TRACKING_INFO_COLLECTION_NAME")

        tracking_id = track_data["awb"]
        tracking_obj = get_tracking_info_doc_using_tracking_id(
            tracking_id, {}, col_name
        )
        if not tracking_obj:
            pushevents_logger.error({"err": "Tracking object not found"})
            return {"success": False}
        return update_status_db(track_data, tracking_obj, is_update)
    except Exception as e:
        pushevents_logger.error({"err": str(e), "awb": str(track_data["awb"])})
        err_list.append(str(e))
        pass
    pushevents_logger.error(
        {"err": str(err_list), "awb": str(track_data["awb"])}
    )
    return {"success": False}


@shared_task
def track_pull_update_bulk_webhook_async(
    track_data_list,
):  # Currently not using this Endpoint, Return only success true or false and log response to prevent celery extra space
    errors_dict = {}
    from .services import validate_track_data

    for track_data in track_data_list:
        try:
            if "EDD" in track_data and track_data["EDD"]:
                track_data["EDD"] = datetime.strptime(
                    track_data["EDD"], "%d-%m-%Y %H:%M"
                )
            else:
                track_data["EDD"] = None
            if "scan_datetime" in track_data and track_data["scan_datetime"]:
                track_data["scan_datetime"] = datetime.strptime(
                    track_data["scan_datetime"], "%d-%m-%Y %H:%M"
                )

            validations = validate_track_data(track_data)
            if "err" in validations:
                errors_dict[track_data["awb"]] = {
                    "error": validations["err"],
                    "data": track_data,
                }
                pass
            if (
                ("scan_type" in track_data)
                and track_data["scan_type"]
                and (track_data["scan_type"] in ["PP", "CC"])
            ):
                pass
            track_pull_update_webhook_async.delay(track_data)

        except Exception as e:
            errors_dict[track_data["awb"]] = {
                "error": str(e),
                "data": track_data,
            }
            pass

    # if errors_dict:
    #     send_email('Errors in aync bulk push track webhook',
    #                str(errors_dict),
    #                ['laxman@pickrr.com', 'gaurav@pickrr.com'])


@shared_task
def task_hit_courier_and_store_response_in_db(batch, courier_account):
    from apps.common.services import hit_courier_and_store_response_in_db

    return hit_courier_and_store_response_in_db(batch, courier_account)


@shared_task
def pull_updates_from_courier_cronjob():
    from apps.common.services import fetch_updates_from_courier

    courier_list = [
        "dtdc",
        "dtdc_air",
        "fedex",
        "fedex_po",
        "fedex_economy",
        "fedex_so",
        "fedex_3kg",
        "fedex_3kg_surface",
    ]
    try:
        fetch_updates_from_courier(courier_list)
        return {"success": True}
    except Exception as e:
        return {"success": False}


@shared_task
def pull_updates_from_courier_without_push_event_cronjob():
    from apps.common.services import fetch_updates_from_courier

    courier_list = [
        "dtdc",
        "dtdc_air",
        "fedex",
        "fedex_po",
        "fedex_economy",
        "fedex_so",
        "fedex_3kg",
        "fedex_3kg_surface",
    ]
    push_event = False
    try:
        fetch_updates_from_courier(courier_list, push_event)
        return {"success": True}
    except Exception as e:
        return {"success": False}


@shared_task
def pull_updates_for_delivered_return_events():
    from apps.common.services import fetch_updates_from_courier

    courier_list = []
    push_event = False
    rt_dl_task = True
    try:
        fetch_updates_from_courier(courier_list, push_event, rt_dl_task)
        return {"success": True}
    except Exception as e:
        return {"success": False}
