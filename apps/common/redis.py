import json

import redis
from django.core.cache import cache

try:
    Redis = redis.Redis(host="0.0.0.0", port="6379", password="")
except Exception as e:
    Redis = []


def check_redis_connection():
    try:
        Redis.get("LXN")
    except (
        redis.exceptions.ConnectionError,
        redis.exceptions.BusyLoadingError,
    ):
        return False
    return True


def check_in_cache(key):
    return Redis.exists(key)


def get_from_cache(key):
    return Redis.get(key)


def check_pattern_in_cache(key):
    return Redis.keys(key)


def get_pattern_from_cache(key):
    return Redis.keys(key)


def update_the_cache(key, value, expired_days=5):
    return Redis.set(key, value, expired_days * 86400)


def delete_from_cache(key):
    return Redis.delete(key)
